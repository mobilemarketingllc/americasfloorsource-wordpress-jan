<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://wordkeeper.com/
 * @since             1.0.0
 * @package           Wordkeeper_Speed
 *
 * @wordpress-plugin
 * Plugin Name:       WordKeeper Speed Optimization
 * Plugin URI:        https://wordkeeper.com/plugins/wordkeeper
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            WordKeeper
 * Author URI:        https://wordkeeper.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wordkeeper-speed
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );

/**
 * This class provides helper methods
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wordkeeper-speed-helpers.php';

/**
 * This class handles the exclusions process with the conflicting plugins.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wordkeeper-speed-exclusions.php';

/**
 * This class provides support for fragment caching
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wordkeeper-speed-fragment-cache.php';
require plugin_dir_path( __FILE__ ) . 'includes/wordkeeper-speed-fragment-cache.php';

/**
 * This class handles the file system based widget cache management.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wordkeeper-speed-widget-cache.php';

/**
 * This class handles the file system based menu cache management.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wordkeeper-speed-menu-cache.php';

/**
 * This class handles the core configurations that are being used throughout the plugin.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wordkeeper-speed-config.php';

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wordkeeper-speed-activator.php
 */
function activate_wordkeeper_speed() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wordkeeper-speed-activator.php';
	Wordkeeper_Speed_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wordkeeper-speed-deactivator.php
 */
function deactivate_wordkeeper_speed() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wordkeeper-speed-deactivator.php';
	Wordkeeper_Speed_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wordkeeper_speed' );
register_deactivation_hook( __FILE__, 'deactivate_wordkeeper_speed' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wordkeeper-speed.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wordkeeper_speed() {

	$plugin = new Wordkeeper_Speed();
	$plugin->run();
}
run_wordkeeper_speed();

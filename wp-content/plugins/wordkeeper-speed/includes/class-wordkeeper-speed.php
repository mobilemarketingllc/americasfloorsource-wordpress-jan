<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://wordkeeper.com/
 * @since      1.0.0
 *
 * @package    Wordkeeper_Speed
 * @subpackage Wordkeeper_Speed/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Wordkeeper_Speed
 * @subpackage Wordkeeper_Speed/includes
 * @author     Lance Dockins <info@wordkeeper.com>
 */
class Wordkeeper_Speed {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Wordkeeper_Speed_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Settings of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      array    $settings    Settings of this plugin.
	 */
	private $settings;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'PLUGIN_NAME_VERSION' ) ) {
			$this->version = PLUGIN_NAME_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'wordkeeper-speed';

		Wordkeeper_Speed_Config::get_instance()->init();
		$this->load_settings();
		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
	}

	/**
	 * Load the required settings for this plugin.
	 *
	 * Include the following settings that make up the plugin:
	 *
	 * - wordkeeper-system-settings. The basic settings for the plugin
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_settings() {
		$this->settings = Wordkeeper_Speed_Config::get_instance()->get_config();
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Wordkeeper_Speed_Loader. Orchestrates the hooks of the plugin.
	 * - Wordkeeper_Speed_i18n. Defines internationalization functionality.
	 * - Wordkeeper_Speed_Admin. Defines all hooks for the admin area.
	 * - Wordkeeper_Speed_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wordkeeper-speed-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wordkeeper-speed-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-wordkeeper-speed-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-wordkeeper-speed-public.php';

		$this->loader = new Wordkeeper_Speed_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Wordkeeper_Speed_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Wordkeeper_Speed_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Wordkeeper_Speed_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

		if($this->settings['wordkeeper']['disable-plugins-update-check'] === true) {
			if(strpos($_SERVER['REQUEST_URI'], 'wp-admin/plugins.php') !== false) {
				$this->loader->add_action('admin_init', $plugin_admin, 'admin_init');

				/*
				* Disable plugin update checks on old WP versions
				*/
				$this->loader->add_action('pre_transient_update_plugins', $plugin_admin, 'last_checked_now');

				/*
				* Disable plugin update checks on new WP versions
				*/
				$this->loader->add_filter('pre_site_transient_update_plugins', $plugin_admin, 'last_checked_now');
			}
		}
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Wordkeeper_Speed_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

		// Capture output for processing before flushed to web server
		if($this->settings['wordkeeper']['use-buffering'] === true) {
			$this->loader->add_action('init', $plugin_public, 'buffer_start', 0);
		}

		$this->loader->add_action('init', $plugin_public, 'heartbeat_control', 0);
		$this->loader->add_filter('heartbeat_settings', $plugin_public, 'heartbeat_frequency');

		// Add resource hints, run late so that other JS/CSS optimizations are done
		if($this->settings['wordkeeper']['use-buffering'] === true && isset($this->settings['wordkeeper']['http-resource-hints']) && $this->settings['wordkeeper']['http-resource-hints'] === true) {
			$this->loader->add_filter('wordkeeper_after_buffer_edit', $plugin_public, 'apply_resource_hints', 100, 1);
		}

		// Minify JavaScript/CSS
		if($this->settings['wordkeeper']['minify'] === true) {
			$this->loader->add_filter('wordkeeper_after_buffer_edit', $plugin_public, 'minify', 10, 1);
			$this->loader->add_action('activated_plugin', $plugin_public, 'activated_plugin', 10, 2);
			$this->loader->add_action('deactivated_plugin', $plugin_public, 'deactivated_plugin', 10, 2);
			$this->loader->add_action('upgrader_process_complete', $plugin_public, 'upgrader_process_complete', 10, 2);
		}

		// Lazy load videos
		if($this->settings['wordkeeper']['lazyload-videos'] === true) {
			if(!$this->settings['wordkeeper']['amp']) {
				$this->loader->add_filter('wordkeeper_before_buffer_edit', $plugin_public, 'lazyload_script_videos', 1, 1);
				$this->loader->add_filter('wordkeeper_buffer_iframes_edit', $plugin_public, 'process_buffer_videos', 2, 2);
			}

			$plugin_public->register_style('wordkeeper-lazyload-video', plugin_dir_url(dirname(__FILE__)) . 'public/css/lazyload.video.min.css');

			if($this->settings['wordkeeper']['lazyload-videos-mode'] == 'default') {
				$plugin_public->register_script('wordkeeper-lazyload-video', plugin_dir_url(dirname(__FILE__)) . 'public/js/lazyload.video.min.js', array('jquery'));
			}
			else {
				$plugin_public->register_style('wordkeeper-lightbox-video', plugin_dir_url(dirname(__FILE__)) . 'public/css/jquery.fancybox.min.css');
				$plugin_public->register_script('wordkeeper-lightbox-video', plugin_dir_url(dirname(__FILE__)) . 'public/js/jquery.fancybox.min.js', array('jquery'));
				$plugin_public->register_script('wordkeeper-lazyload-video', plugin_dir_url(dirname(__FILE__)) . 'public/js/lightbox.video.min.js', array('jquery'));
			}
		}

		// Lazy load images
		if($this->settings['wordkeeper']['lazyload-images'] === true) {
			if(!$this->settings['wordkeeper']['amp']) {
				$this->loader->add_filter('wordkeeper_buffer_images_edit', $plugin_public, 'process_buffer_images', 2, 2);
			}

			$plugin_public->register_script('wordkeeper-lazyload', plugin_dir_url(dirname(__FILE__)) . 'public/js/lazysizes.min.js', array('jquery'));
			if($this->settings['wordkeeper']['lazyload-mode'] != 'near') {
				if(function_exists('wp_add_inline_script')) {
					if($this->settings['wordkeeper']['lazyload-mode'] == 'none') {
						$lazymode = "window.lazySizesConfig = window.lazySizesConfig || {};\nlazySizesConfig.loadMode = 0;";
					}
					else {
						$lazymode = "window.lazySizesConfig = window.lazySizesConfig || {};\nlazySizesConfig.loadMode = 1; lazySizesConfig.expand = 50;";
					}
					wp_add_inline_script('wordkeeper-lazyload', $lazymode, 'before');
					Wordkeeper_Speed_Config::get_instance()->update_config('lazy-loaded', true);
					$this->settings = Wordkeeper_Speed_Config::get_instance()->get_config();
					unset($lazymode);
				}
			}
		}

		// Lazy load iframes
		if($this->settings['wordkeeper']['lazyload-iframes'] === true) {
			if(!$this->settings['wordkeeper']['amp']) {
				$this->loader->add_filter('wordkeeper_buffer_iframes_edit', $plugin_public, 'process_buffer_iframes', 2, 2);
			}

			$plugin_public->register_script('wordkeeper-lazyload', plugin_dir_url(dirname(__FILE__)) . 'public/js/lazysizes.min.js', array('jquery'));
			if(!isset($wordkeeper['lazy-loaded'])) {
				if($this->settings['wordkeeper']['lazyload-mode'] != 'near') {
					if(function_exists('wp_add_inline_script')) {
						if($this->settings['wordkeeper']['lazyload-mode'] == 'none') {
							$lazymode = "window.lazySizesConfig = window.lazySizesConfig || {};\nlazySizesConfig.loadMode = 0;";
						}
						else {
							$lazymode = "window.lazySizesConfig = window.lazySizesConfig || {};\nlazySizesConfig.loadMode = 1; lazySizesConfig.expand = 50;";
						}
						wp_add_inline_script('wordkeeper-lazyload', $lazymode, 'before');
						unset($lazymode);
					}
				}
			}
		}

		// Defer JavaScript if not an AMP page
		if($this->settings['wordkeeper']['defer-javascript'] === true) {
			if(!$this->settings['wordkeeper']['amp']) {
				$this->loader->add_filter('wordkeeper_buffer_scripts_edit', $plugin_public, 'defer_buffer_scripts', 2, 2);
			}
		}

		// Use custom deferscript loader to async load problematic scripts
		if($this->settings['wordkeeper']['use-deferscript'] === true) {
			if(!$this->settings['wordkeeper']['amp']) {
				$this->loader->add_filter('wordkeeper_after_buffer_edit', $plugin_public, 'apply_deferscript', 1, 1);
			}
		}

		// Optimize/concatenate Google fonts
		if($this->settings['wordkeeper']['optimize-google-fonts'] === true) {
			$this->loader->add_filter('wordkeeper_before_buffer_edit', $plugin_public, 'concatenate_google_fonts', 2, 1);
		}

		// Strip duplicate JS and CSS files
		if($this->settings['wordkeeper']['strip-duplicate-assets'] === true) {
			$this->loader->add_filter('wordkeeper_before_buffer_edit', $plugin_public, 'strip_duplicate_assets', 2, 1);
		}

		// Strip HTML comments
		if($this->settings['wordkeeper']['strip-comments'] === true) {
			$this->loader->add_filter('wordkeeper_before_buffer_edit', $plugin_public, 'strip_comments', 3, 1);
		}

		// Strip PHPSESSID from headers if present
		if($this->settings['wordkeeper']['strip-session-cookie'] === true) {
			$this->loader->add_action('send_headers', $plugin_public, 'strip_session_cookie');
		}

		// Remove Google Maps API output
		if($this->settings['wordkeeper']['disable-maps-api'] === true) {
			$this->loader->add_filter('wordkeeper_before_buffer_edit', $plugin_public, 'remove_google_maps', 4, 1);
		}

		// Remove query strings from static resources
		if($this->settings['wordkeeper']['remove-query-strings'] === true) {
			$this->loader->add_filter('script_loader_src', $plugin_public, 'remove_query_strings', 15, 1);
			$this->loader->add_filter('style_loader_src', $plugin_public, 'remove_query_strings', 15, 1);
		}

		// Remove Emoji support
		if($this->settings['wordkeeper']['disable-emojis'] === true) {
			$this->loader->add_action('init', $plugin_public, 'disable_emojis');
			$this->loader->add_filter('tiny_mce_plugins', $plugin_public, 'disable_tinymce_emojis');
			$this->loader->add_filter('wp_resource_hints', $plugin_public, 'disable_emojis_dns_prefetch', 10, 2);
		}

		// Remove embed support
		if($this->settings['wordkeeper']['disable-wordpress-embeds'] === true) {
			$this->loader->add_action('init', $plugin_public, 'disable_embeds', 9999);
			$this->loader->add_filter('tiny_mce_plugins', $plugin_public, 'disable_tinymce_embeds');
			$this->loader->add_filter('rewrite_rules_array', $plugin_public, 'disable_embed_rewrites');
		}

		// Clean up various forms of garbage output that jeopardizes security and speed
		if($this->settings['wordkeeper']['disable-header-garbage'] === true) {
			$this->loader->add_filter('the_generator', $plugin_public, 'remove_wordpress_generator');
			remove_action('wp_head', 'wp_generator');
			remove_action('wp_head', 'wlwmanifest_link');
			remove_action('wp_head', 'rsd_link');
			remove_action('wp_head', 'wp_shortlink_wp_head');
			remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');

			// Remove WooCommerce generator
			$this->loader->add_action('get_header', $plugin_public, 'remove_woocommerce_generator');

			// Remove Visual Composer generator
			$this->loader->add_action('init', $plugin_public, 'remove_visual_composer_generator', 100);

			// Remove RevSlider generator
			$this->loader->add_filter('revslider_meta_generator', $plugin_public, 'remove_revslider_generator');
		}

		// Optimize pagination queries by replacing SQL_CALC_ROWS
		if($this->settings['wordkeeper']['optimize-db-count-queries'] === true) {
			// Sets no found rows to replace SQL_CALC_ROWS
			$this->loader->add_filter('pre_get_posts', $plugin_public, 'db_set_no_found_rows', 10, 1);

			// Sets replacement for SQL_CALC_ROWS to COUNT * and accounts for paginated queries
			$this->loader->add_filter('posts_clauses', $plugin_public, 'db_set_found_posts', 10, 2);
		}

		// Remove WooCommerce get_refreshed_fragments calls from occurring by default
		if($this->settings['wordkeeper']['disable-woocommerce-refresh-fragments'] === true) {
			// Only run if WooCommerce is active
			if(in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
				/** Disable Ajax Call from WooCommerce */
				$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'dequeue_woocommerce_cart_fragments', 11);
			}
		}

		// Ported here from wordkeeper-speed-fixes.php in the old version of WordKeeper Speed.
		$this->loader->add_filter('wordkeeper_after_buffer_edit', $plugin_public, 'custom_defer');

		if($this->settings['wordkeeper']['cache-widgets'] === true) {
			$this->loader->add_filter('widget_display_callback', $plugin_public, 'cache_widget_output', 10, 3);
			$this->loader->add_action('in_widget_form', $plugin_public, 'show_widget_cache_options', 5, 3);
			$this->loader->add_filter('widget_update_callback', $plugin_public, 'widget_update_callback', 5, 4);
			$this->loader->add_action('delete_widget', $plugin_public, 'delete_widget', 5, 3);
		}

		if($this->settings['wordkeeper']['partials'] === true){
			$this->loader->add_action( 'wp_before_admin_bar_render', $plugin_public, 'admin_bar_purge_cache' );
			$this->loader->add_action( 'wp_loaded', $plugin_public, 'post_processing_after_page_loaded');
			$this->loader->add_action( 'update_option_permalink_structure' , $plugin_public, 'permalinks_updated');
		}

		if($this->settings['wordkeeper']['cache-menus'] === true) {
			//$this->loader->add_filter( 'pre_wp_nav_menu', $plugin_public, 'pre_wp_nav_menu', 10, 2 );
			$this->loader->add_filter( 'wp_nav_menu', $plugin_public, 'wp_nav_menu', 10, 2);
			$this->loader->add_action( 'wp_update_nav_menu', $plugin_public, 'wp_update_nav_menu', 10, 1);
			$this->loader->add_action( 'update_option_permalink_structure' , $plugin_public, 'permalinks_updated');
		}

		if(
			(isset($this->settings['wordkeeper']['enable-scheduled-transient-purges']) && $this->settings['wordkeeper']['enable-scheduled-transient-purges'] === true) ||
			(isset($this->settings['wordkeeper']['disable-slow-woocommerce-transient-purges']) && $this->settings['wordkeeper']['disable-slow-woocommerce-transient-purges'] === true)
		) {

			// Schedule the CRON Job to purge all expired transients
			if (!wp_next_scheduled('purge_popup_transients_cron')) {
				/**
				 * wp_schedule_event
				 *
				 * @param - When to start the CRON job
				 * @param - The interval in for each subsequent run
				 * @param - The name of the CRON JOB
				 */
				wp_schedule_event( time(), 'hourly', 'purge_popup_transients_cron');

			}
			$this->loader->add_action( 'purge_popup_transients_cron',  $plugin_public, 'purge_transients', 10 , 1);

		}

		if(isset($this->settings['wordkeeper']['disable-slow-woocommerce-transient-purges']) && $this->settings['wordkeeper']['disable-slow-woocommerce-transient-purges'] === true){
			include_once ABSPATH . 'wp-admin/includes/plugin.php';
			if ( is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
				$this->loader->add_filter( 'woocommerce_delete_version_transients_limit', $plugin_public, 'filter_woocommerce_delete_version_transients_limit', 10, 1 );
			}
		}

		if($this->settings['wordkeeper']['async-woocommerce-emails'] === true) {
			$this->loader->add_filter('woocommerce_defer_transactional_emails', $plugin_public, 'woocommerce_defer_transactional_emails');
		}

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Wordkeeper_Speed_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}

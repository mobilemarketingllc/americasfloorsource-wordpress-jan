<?php

/**
 * This class provides support for fragment caching
 *
 * @package:     WordKeeper Speed Fragment Cache
 * @author:  WordKeeper
 * @Author URI: http://www.wordkeeper.com
 *
 */
class WordKeeper_Speed_Fragment_Cache {
	public $key = '';
	public $ttl = '';
	public $role_based = false;
	public $cache_path = ABSPATH . 'wp-content/cache/partials/fragments/';

	/**
	 * Returns the fragment cache path
	 *
	 * @access public
	 * @param void
	 * @return mixed
	 */
	public function get_cache_path(){
		if ( is_multisite() ){
			return ABSPATH . 'wp-content/cache/' . get_current_blog_id() . '/partials/fragments/';
		}
		return ABSPATH . 'wp-content/cache/partials/fragments/';
	}


	/**
	 * __construct function.
	 *
	 * @access public
	 * @param mixed $key
	 * @param mixed $ttl
	 * @param mixed $role_based
	 * @return void
	 */
	public function __construct($key, $ttl, $role_based) {
		$this->key = $key;
		$this->ttl = $ttl;
		$this->role_based = $role_based;
	}

	/**
	 * Generates and returns hashed key
	 *
	 * @access public
	 * @param void
	 * @return mixed
	 */
	public function get_cache_key() {
		$key = array();
		$key[] = md5($this->key);

		if($this->role_based){
			$user = wp_get_current_user();
			$role = $user->roles ? $user->roles[0] : false;
			if(empty($role)) {
				$role = 'guest';
			}
			$key[] = $role;
		}

		$cache_key = implode('.', $key);
		return $cache_key;
	}

	/**
	 * Checks to if cache data is available. If cache available then returns it, else starts buffering
	 *
	 * @access public
	 * @param void
	 * @return mixed
	 */
	public function available() {
		if(!file_exists($this->get_cache_path() . $this->get_cache_key() . '.dat')){
			ob_start();
			return false;
		}

		$cache = trim(file_get_contents($this->get_cache_path() . $this->get_cache_key() . '.dat'));
		if($cache){
			$cache = $this->decode($cache);
			$cache = $cache[0];
			$cache_data = $this->decode($cache['cache_data']);
			$cache_data = $cache_data[0];

			$now = strtotime('now');
			$mtime = filemtime($this->get_cache_path() . $this->get_cache_key() . '.dat');
			if(($now - $mtime) > $cache['cache_expiry']) {
				ob_start();
				return false;
			}
			echo $cache_data;
		}
	}

	/**
	 * End bufering and save cache data
	 *
	 * @access public
	 * @param void
	 * @return void
	 */
	public function save() {
		$output = ob_get_flush();

		$cache_data = $this->encode($output);
		$cache_time = strtotime('now');
		$cache = array(
			'cache_data' => $cache_data,
			'cache_time' => $cache_time,
			'cache_expiry' => $this->ttl
		);

		if(!$this->cache_path_exist()){
			$this->create_cache_path();
		}

		file_put_contents($this->get_cache_path() . $this->get_cache_key() . '.dat', $this->encode($cache));
	}

	/**
	 * Checks if the cache path exists, if not then creates it
	 *
	 * @access public
	 * @param void
	 * @return bool
	 */
	public function cache_path_exist() {
		if (!is_dir($this->get_cache_path())) {
			return false;
		}
		return true;
	}

	/**
	 * Creates the cache path
	 *
	 * @access public
	 * @param void
	 * @return void
	 */
	public function create_cache_path() {
		mkdir($this->get_cache_path(), 0775, true);
	}

	/**
	 * Purges all cached data
	 *
	 * @access static
	 * @param void
	 * @return void
	 */
	public static function purge_all($folder) {
		if ( is_multisite() ){
			$path = ABSPATH . 'wp-content/cache/' . get_current_blog_id() . '/partials/' . $folder . '/*';
		}
		else{
			$path = ABSPATH . 'wp-content/cache/partials/' . $folder . '/*';
		}

		$files = glob($path); //get all file names
		if(empty($files) || !$files){
			return false;
		}

		foreach($files as $file){
			if(is_file($file)) {
				unlink($file); //delete file
			}
		}
	}

	/**
	 * Encodes given data
	 *
	 * @access public
	 * @param mixed $data
	 * @return mixed
	 */
	public function encode($data) {
		return base64_encode(serialize(array($data)));
	}

	/**
	 * Decodes given data
	 *
	 * @access public
	 * @param mixed $data
	 * @return mixed
	 */
	public function decode($data) {
		return unserialize(base64_decode($data));
	}
}

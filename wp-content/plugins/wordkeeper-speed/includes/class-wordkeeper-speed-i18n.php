<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://wordkeeper.com/
 * @since      1.0.0
 *
 * @package    Wordkeeper_Speed
 * @subpackage Wordkeeper_Speed/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Wordkeeper_Speed
 * @subpackage Wordkeeper_Speed/includes
 * @author     Lance Dockins <info@wordkeeper.com>
 */
class Wordkeeper_Speed_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'wordkeeper-speed',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}

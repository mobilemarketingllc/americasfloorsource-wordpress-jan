<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://wordkeeper.com/
 * @since      1.0.0
 *
 * @package    Wordkeeper_Speed
 * @subpackage Wordkeeper_Speed/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Wordkeeper_Speed
 * @subpackage Wordkeeper_Speed/public
 * @author     WordKeeper <wordkeeper@test.com>
 */
class Wordkeeper_Speed_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Settings of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      array    $settings    Settings of this plugin.
	 */
	private $settings;

	/**
	 * Frontend styles of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      array    $styles    Frontend styles of this plugin.
	 */
	private $styles;

	/**
	 * Frontend scripts of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      array    $scripts    Frontend scripts of this plugin.
	 */
	private $scripts;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->settings = Wordkeeper_Speed_Config::get_instance()->get_config();
	}

	/**
	 * Register a stylesheet for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function register_style($handle, $style, $deps = array()) {
		$this->styles[] = array(
			'handle' => $handle,
			'style' => $style,
			'deps' => $deps
		);
	}

	/**
	 * Register a JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function register_script($handle, $script, $deps = array()) {
		$this->scripts[] = array(
			'handle' => $handle,
			'script' => $script,
			'deps' => $deps
		);
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		if(empty($this->styles) || !is_array($this->styles))
			return;

		foreach($this->styles as $style) {
			wp_enqueue_style( $style['handle'], $style['style'], $style['deps'], $this->version, 'all' );
		}

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		if(empty($this->scripts) || !is_array($this->scripts))
			return;

		foreach($this->scripts as $script) {
			wp_enqueue_script( $script['handle'], $script['script'], $script['deps'], $this->version, false );
		}

	}

	/**
	 * wordkeeper_buffer_start function.
	 *
	 * Start output buffering
	 *
	 * @access public
	 * @return void
	 */
	function buffer_start() {
		if (!is_admin()) {
			ob_start(array($this, 'buffer_callback'));
		}
	}

	/**
	 * buffer_callback function.
	 *
	 * @access public
	 * @param mixed $buffer
	 * @return void
	 */
	function buffer_callback($buffer) {
		//global $wordkeeper;

		// Check for a Content-Type header. Currently only apply rewriting to "text/html" or undefined
		$headers = headers_list();
		$content_type = null;

		foreach($headers as $header) {
			if(strpos(strtolower($header), 'content-type:') === 0) {
				$pieces = explode(':', strtolower($header));
				$content_type = trim($pieces[1]);
				break;
			}
		}

		// Only apply buffer filters to text/html pages
		if(is_null($content_type) || substr($content_type, 0, 9) === 'text/html') {

			// Apply any pre-processing full buffer filters
			apply_filters_ref_array('wordkeeper_before_buffer_edit', array(&$buffer));

			$resources = array();
			$scripts = array();
			$noscripts = array();
			$resources['scripts'] = $scripts;
			$resources['noscripts'] = $noscripts;

			/*preg_match_all('/(?s)<\s?noscript.*?(?:\/\s?>|<\s?\/\s?noscript\s?>)/', $buffer, $noscripts);*/
			preg_match_all("/<\s?noscript[^>]*\s?>(.*)<\s?\/\s?noscript\s?>/Uis", $buffer, $noscripts);
			if(!empty($noscripts)) {
				$resources['noscripts'] = $noscripts[0];
			}

			/*preg_match_all('/(?s)<\s?script.*?(?:\/\s?>|<\s?\/\s?script\s?>)/', $buffer, $scripts);*/
			preg_match_all("/<\s?script[^>]*\s?>(.*)<\s?\/\s?script\s?>/Uis", $buffer, $scripts);
			if(!empty($scripts)) {
				$resources['scripts'] = $scripts[0];
				$resources['scripts'] = $this->filter_script_tags($resources['scripts'], $buffer);

				// Apply any script and video related filters
				apply_filters_ref_array('wordkeeper_buffer_scripts_edit', array(&$buffer, &$resources));
			}

			// Capture all anchors
			preg_match_all('/(?s)<\s?a.*?(?:\/\s?>|<\s?\/\s?a\s?>)/', $buffer, $matched_anchors);
			$anchors = $matched_anchors[0];
			$this->filter_anchors($buffer, $anchors);

			// Capture header tag
			preg_match_all('#<\s*?header\b[^>]*>(.*?)</header\b[^>]*>#s', $buffer, $matched_header);
			/*preg_match_all('/(?s)<\s?header.*?(?:\/\s?>|<\s?\/\s?header\s?>)/', $buffer, $matched_header);*/
			$header = $matched_header[0];
			$this->filter_header($buffer, $header);


			// Capture iframes for video optimization
			if($this->settings['wordkeeper']['capture-iframes'] && !$this->settings['wordkeeper']['amp']) {
				preg_match_all('/(?s)<\s?iframe.*?(?:\/\s?>|<\s?\/\s?iframe\s?>)/', $buffer, $iframes);

				$resources['iframes'] = $iframes[0];

				// Filter out iframes in <noscript> tags
				$this->filter_noscript('iframes', $resources);

				// Filter iframes to segregate iframes from videos
				$this->filter_iframes($resources);

				// Apply any iframe and video related filters
				apply_filters_ref_array('wordkeeper_buffer_iframes_edit', array(&$buffer, &$resources));
			}

			// Capture images for lazy loading
			if($this->settings['wordkeeper']['capture-images'] && !$this->settings['wordkeeper']['amp']) {
				preg_match_all('/(?s)<\s?img[^>]*>/', $buffer, $images);

				if(!empty($images)) {
					$resources['images'] = $images[0];

					// Filter out images in <noscript> tags
					$this->filter_noscript('images', $resources);

					// Apply any image related filters
					apply_filters_ref_array('wordkeeper_buffer_images_edit', array(&$buffer, &$resources));
				}
			}

			// Capture scripts for JS/video optimization
			if($this->settings['wordkeeper']['capture-links']) {
				preg_match_all('/(?s)<\s?link.*?(?:\/\s?>|<\s?\/\s?link\s?>)/', $buffer, $links);
				if(!empty($links)) {
					$resources['links'] = $links[0];

					// Filter out iframes in <noscript> tags
					$this->filter_noscript('links', $resources);

					// Apply any link related filters
					apply_filters_ref_array('wordkeeper_buffer_links_edit', array(&$buffer, &$resources));
				}
			}
		}

		// Apply any post processing full buffer filters
		//apply_filters_ref_array('wordkeeper_after_buffer_edit', array(&$buffer));
		$buffer = apply_filters('wordkeeper_after_buffer_edit', $buffer);
		return $buffer;
	}


	/**
	 * filter_script_tags function.
	 *
	 * @access public
	 * @param mixed $scripts
	 * @param mixed &$buffer
	 * @return void
	 */
	function filter_script_tags($scripts, &$buffer) {
		// Add cf-async to lazysizes script so that CloudFlare's RocketLoader cannot mess with it.
		for ($i = 0; $i < count($scripts); $i++){
			if(strpos($scripts[$i], 'lazysizes') !== false) { // && strpos($scripts[$i], 'cf-async') == false){
				$modified = str_replace('<script ', '<script cf-async="true" ', $scripts[$i]);
				$buffer = str_replace($scripts[$i], $modified, $buffer);
			}
		}
		return $scripts;
	}

	/**
	 * filter_anchors function.
	 *
	 * @access public
	 * @param mixed &$buffer
	 * @param mixed &$anchors
	 * @return void
	 */
	function filter_anchors(&$buffer, &$anchors) {
		$site_url = get_site_url();
		foreach($anchors as $i => $a){
			preg_match('# href=[\'"]*([^\'"]+)[\'"]#', $a, $_anchor);

			// Found home page link. Check if it's logo
			if(isset($_anchor[1]) && trim($_anchor[1], '/') == trim($site_url, '/')){
				preg_match_all('/(?s)<\s?img[^>]*>/', $a, $images);
				if(!empty($images[0])){
					foreach($images[0] as $img){
						// This is probably the logo image. Add data-skip-lazyload to it.
						if(strpos($img, 'data-skip-lazyload') !== false){
							// Already skipped for lazyload, don't do anything here.
						}
						else{
							$new_img = str_replace('<img', '<img data-skip-lazyload', $img);
							$buffer = str_replace($img, $new_img, $buffer);
						}
					}
				}
			}
			else{
				unset($anchors[$a]);
			}
		}
	}

	/**
	 * filter_header function.
	 *
	 * @access public
	 * @param mixed &$buffer
	 * @param mixed &$header
	 * @return void
	 */
	function filter_header(&$buffer, &$header) {
		foreach($header as $i => $h){
			preg_match_all('/(?s)<\s?img[^>]*>/', $h, $images);
			if(!empty($images[0])){
				foreach($images[0] as $img){
					preg_match('# src=[\'"]*([^\'"]+)[\'"]#', $img, $src);
					if(strpos($src[1], 'logo') !== false){
						// This is probably the logo image. Add data-skip-lazyload to it.
						if(strpos($img, 'data-skip-lazyload') !== false){
							// Already skipped for lazyload, don't do anything here.
						}
						else{
							$new_img = str_replace('<img', '<img data-skip-lazyload', $img);
							$buffer = str_replace($img, $new_img, $buffer);
						}
					}
				}
			}
		}
	}


	/**
	 * filter_iframes function.
	 *
	 * @access public
	 * @param mixed &$resources
	 * @return void
	 */
	function filter_iframes(&$resources) {
		//global $wordkeeper;

		$resources['videos'] = array();

		if(!empty($resources['iframes'])) {
			foreach ($resources['iframes'] as $index => $iframe) {
				if($this->settings['wordkeeper']['lazyload-videos'] || $this->settings['wordkeeper']['lazyload-iframes']) {
					if(strpos($iframe, 'youtube.com/embed/videoseries') || strpos($iframe, 'youtube.com/embed/live_stream')){
						// Don't want to process this type of YouTube embeds
						continue;
					}
					else if(
							strpos($iframe, 'youtube.com/embed/') ||
							strpos($iframe, 'youtu.be/')
						) {
							unset($resources['iframes'][$index]);
							$resources['videos'][] = array('video' => $iframe, 'type' => 'youtube');
						}
					elseif(strpos($iframe, 'https://player.vimeo.com/video/') !== false) {
						unset($resources['iframes'][$index]);
						$resources['videos'][] = array('video' => $iframe, 'type' => 'vimeo');
					}
					elseif(strpos($iframe, '//fast.wistia.net/embed/iframe/') !== false) {
						unset($resources['iframes'][$index]);
						$resources['videos'][] = array('video' => $iframe, 'type' => 'wistia');
					}
					elseif(strpos($iframe, 'gform_ajax_frame') !== false) {
						// Skip Gravity Forms iframe
						continue;
					}
				}
			}
		}
	}


	/**
	 * filter_noscript function.
	 *
	 * @access public
	 * @param mixed $type
	 * @param mixed &$resources
	 * @return void
	 */
	function filter_noscript($type, &$resources) {
		$tags = array(
			'iframes' => 'iframe',
			'images' => 'img',
			'links' => 'link'
		);

		foreach($resources['noscripts'] as $noscript) {
			if(strpos($noscript, '<' . $tags[$type]) !== false) {
				foreach ($resources[$type] as $i => $value){
					if(strpos($noscript, $value) !== false) {
						unset($resources[$type][$i]);
					}
				}
			}
		}

		foreach($resources['scripts'] as $script) {
			if(strpos($script, '<' . $tags[$type]) !== false) {
				foreach ($resources[$type] as $i => $value){
					if(strpos($script, $value) !== false) {
						unset($resources[$type][$i]);
					}
				}
			}
		}
	}


	/**
	 * lazyload_video function.
	 *
	 * @access public
	 * @param mixed $video
	 * @param mixed &$buffer
	 * @return void
	 */
	function lazyload_video($video, &$buffer) {
		//global $wordkeeper;

		if(strpos($video['video'], 'data-skip-lazyload') !== false) {
			return;
		}

		preg_match('#src=[\'"]([^\'"]*)[\'"]#', $video['video'], $src);

		$src = explode('?', $src[1]);
		$src = trim($src[0], '/');
		$src = str_replace('https://', '', $src);
		$src = str_replace('http://', '', $src);
		$src = str_replace('//', '', $src);

		switch($video['type']) {
		case 'youtube':
			$src = str_replace('www.youtube.com/embed/', '', $src);
			$src = str_replace('youtube.com/embed/', '', $src);
			$src = str_replace('youtu.be/', '', $src);
			$src = explode('/', $src);
			$embed = $src[0];
			$datasrc = 'https://www.youtube.com/watch?v=' . $embed . '&autoplay=1&rel=0&controls=0&showinfo=0';

			if($embed == 'videoseries') {
				return;
			}
			break;
		case 'vimeo':
			$src = str_replace('player.vimeo.com/video/', '', $src);
			$src = explode('/', $src);
			$embed = $src[0];
			$datasrc = 'https://vimeo.com/' . $embed . '?color=949494&title=0&byline=0&portrait=0&autoplay=1';
			break;
		case 'wistia':
			$src = str_replace('fast.wistia.net/embed/iframe/', '', $src);
			$src = trim($src, '/');
			$src = explode('/', $src);
			$embed = $src[0];
			$datasrc = 'javascript:;';
			break;
		default:
			break;
		}

		preg_match('#width=[\'"]*([0-9%]+)[\'"]*#', $video['video'], $width);
		preg_match('#class=[\'"]*([^\'"]+)[\'"]*#', $video['video'], $class);

		$width = (!empty($width)) ? $width[1] : null;
		$styles = '';

		if($this->settings['wordkeeper']['lazyload-videos-mode'] == 'default') {
			$template = trim(file_get_contents(dirname(__FILE__) . '/templates/lazyload.video.html'));
		}
		else {
			$template = trim(file_get_contents(dirname(__FILE__) . '/templates/lightbox.video.html'));
		}
		$replace = str_replace('{class}', ' ' . $class[1], $template);
		$replace = str_replace('{type}', $video['type'], $replace);
		$replace = str_replace('{embed}', $embed, $replace);

		if(!empty($width) && strpos($width, '%') === false) {
			$widthtype = ($width >= 525) ? 'max-width' : 'min-width';
			$styles .= ' ' . $widthtype . ': ' . $width . 'px;';
		}
		elseif(!empty($width) && strpos($width, '%') !== false) {
			$styles .= ' width: ' . $width . '%;';
		}

		$replace = str_replace('{styles}', 'style="' . trim($styles) . '"', $replace);
		$replace = str_replace('{video}', $datasrc, $replace);

		if($video['type'] == 'wistia') {
			$replace = str_replace('data-fancybox', 'data-fancybox="iframe" data-type="iframe" data-src="//fast.wistia.net/embed/iframe/' . $embed . '?autoplay=1"', $replace);
		}

		$buffer = str_replace($video['video'], $replace, $buffer);
	}


	/**
	 * lazyload_image function.
	 *
	 * @access public
	 * @param mixed $image
	 * @param mixed &$buffer
	 * @return void
	 */
	function lazyload_image($image, &$buffer) {
		if($this->is_image_partially_excluded($image, $buffer)) {
			return $buffer;
		}

		$replaceimage = $image;

		// Handle src
		preg_match('#[\s\t\r\n]src=[\'"]?([^\'"]*)[\'"]?#', $image, $src);

		if(strpos($image, 'data-src') !== false) {
			preg_match('#data-src=[\'"]?([^\'"]*)[\'"]?#', $image, $datasrc);

			if(!empty($datasrc) && !empty($datasrc[1])) {
				if(!empty($src) && !empty($src[1]) && strpos($src[1], '//') !== false) {
					//$url = $src[1];
					$replaceimage = str_replace($datasrc[0], '', $replaceimage);
				}
				elseif(!empty($src)) {
					//$url = $datasrc[1];
					$replaceimage = str_replace($src[0], '', $replaceimage);
					$replaceimage = str_replace('data-src', 'src', $replaceimage);
				}
				else {
					$replaceimage = str_replace('data-src', 'src', $replaceimage);
				}
			}
			$replaceimage = str_replace('src=', 'data-src=', $replaceimage);
		}
		else {
			$replaceimage = str_replace('src=', 'data-src=', $replaceimage);
			$replaceimage = str_replace('<img', '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mP8Xw8AAoMBgDTD2qgAAAAASUVORK5CYII="', $replaceimage);
		}

		// Handle srcset
		preg_match('#[\s\t\r\n]srcset=[\'"]?([^\'"]*)[\'"]?#', $image, $src);

		if(strpos($image, 'data-srcset') !== false) {
			preg_match('#data-srcset=[\'"]?([^\'"]*)[\'"]?#', $image, $datasrc);

			if(!empty($datasrc) && !empty($datasrc[1])) {
				if(!empty($src) && !empty($src[1]) && strpos($src[1], '//') !== false) {
					//$url = $src[1];
					$replaceimage = str_replace($datasrc[0], '', $replaceimage);
				}
				elseif(!empty($src)) {
					//$url = $datasrc[1];
					$replaceimage = str_replace($src[0], '', $replaceimage);
					$replaceimage = str_replace('data-srcset', 'srcset', $replaceimage);
				}
				else {
					$replaceimage = str_replace('data-srcset', 'srcset', $replaceimage);
				}
			}
			$replaceimage = str_replace('srcset=', 'data-srcset=', $replaceimage);
		}
		else {
			$replaceimage = str_replace('srcset=', 'data-srcset=', $replaceimage);
		}

		if(strpos($image, 'class=') === false) {
			$replaceimage = str_replace('<img', '<img class="lazyload"', $replaceimage);
		}
		else {
			preg_match('#class=[\'"]?([^\'"]*)[\'"]?#', $image, $class);
			if(!empty($class) && !empty($class[1])) {
				$classes = explode(' ', $class[1]);
				$classes[] = 'lazyload';
				$replaceimage = str_replace($class[0], 'class="' . implode(' ', $classes) . '"', $replaceimage);
			}
			else{
				$replaceimage = str_replace($class[0], 'class="lazyload"', $replaceimage);
			}
		}

		$buffer = str_replace($image, $replaceimage, $buffer);
	}

	/**
	 * is_image_partially_excluded function.
	 *
	 * @access public
	 * @param mixed $image
	 * @return bool
	 */
	function is_image_partially_excluded($image, $buffer) {
		$excluded = false;

		// Exclude this image if the src is empty
		if(strpos($image, 'src=""') !== false){
			return true;
		}

		/**
		 * To add exclusions, there are two possible ways.
		 *
		 * 1) if you have just one unique condition i.e. a class name that you want to match inside the current <img> tag, just add it directly as a string.
		 * 2) if you have a set of rules which ALL have to be true in order to make sure you're excluding the correct element, add them as an array. That array can have two types of targets.
		 *  2.1) image: add items to this index if you want to compare them within the <img> tag
		 *  2.2) buffer: add items to this index if you want to compare with the whole output buffer for precision
		 */
		$exclusions = array(
			// WordKeeper Core
			'data-skip-lazyload',

			// Revolution Slider
			'rev-slidebg',

			// Layer Slider
			'ls-bg', 'ls-tn',

			// Angular has ng-src attribute. We should skip this as well.
			'ng-src',

			// Nivo Slider
			array(
				'image' => array('attachment-full'),
				'buffer' => array('nivoSlider')
			),
			array(
				'image' => array('slide-', 'slider-'),
				'buffer' => array('nivoSlider')
			),
			array(
				'image' => array('nivo-main-image'),
				'buffer' => array('nivoSlider')
			),

			// Coin Slider
			array(
				'image' => array('slider-', 'slide-'),
				'buffer' => array('coin-slider')
			),

			array(
				'image' => array('masterslider'),
				'buffer' => array('master-slider')
			),

			// Essential Grid
			array(
				'buffer' => array('esg-grid')
			),

			array(
				'buffer' => array('woocommerce-product-gallery__image')
			)
		);

		foreach($exclusions as $e){

			// Check to see if the exclusion type is an array
			if(is_array($e)){

				/**
				 * There can multiple scenarios. There might be a situation where both ['image'] and ['buffer'] are not supplied but only one of them. So deal with them separately.
				 */

				$image_conditions_passed = false;
				$image_conditions_passed_counter = 0;
				$buffer_conditions_passed = false;
				$buffer_conditions_passed_counter = 0;

				if(isset($e['image']) && count($e['image']) > 0){
					foreach($e['image'] as $k){
						if(strpos($image, $k) !== false){
							$image_conditions_passed_counter++;
						}
					}
					if(count($e['image']) == $image_conditions_passed_counter){
						$image_conditions_passed = true;
					}
				}
				if(isset($e['buffer']) && count($e['buffer']) > 0){
					foreach($e['buffer'] as $k){
						if(strpos($buffer, $k) !== false){
							$buffer_conditions_passed_counter++;
						}
					}
					if(count($e['buffer']) == $buffer_conditions_passed_counter){
						$buffer_conditions_passed = true;
					}
				}

				// Now check first if both were supplied for exclusion rules
				if(isset($e['image']) && count($e['image']) > 0 && isset($e['buffer']) && count($e['buffer']) > 0){
					if($image_conditions_passed && $image_conditions_passed_counter > 0 && $buffer_conditions_passed && $buffer_conditions_passed_counter > 0){
						$excluded = true;
					}
				}

				// If one of them applies then one of them needs to be true
				else if( (isset($e['image']) && count($e['image']) > 0) || (isset($e['buffer']) && count($e['buffer']) > 0) ){
						if( ($image_conditions_passed && $image_conditions_passed_counter > 0) || ($buffer_conditions_passed && $buffer_conditions_passed_counter > 0) ){
							$excluded = true;
						}
					}
			}

			// Regular string based exclusions
			else if(strpos($image, $e) !== false){
					$excluded = true;
					break;
				}
		}
		return $excluded;
	}


	/**
	 * lazyload_iframe function.
	 *
	 * @access public
	 * @param mixed $iframe
	 * @param mixed &$buffer
	 * @return void
	 */
	function lazyload_iframe($iframe, &$buffer) {
		if(strpos($iframe, 'data-skip-lazyload') !== false) {
			return;
		}

		if(strpos($iframe, 'calendar.google.com/calendar/embed') !== false) {
			return;
		}

		preg_match('#[\s\t\r\n]src=[\'"]?([^\'"]*)[\'"]?#', $iframe, $src);

		if(strpos($iframe, 'data-src') !== false) {
			preg_match('#data-src=[\'"]?([^\'"]*)[\'"]?#', $iframe, $datasrc);

			// If both src and data-src are empty, don't process the iframe
			if((empty($datasrc) || empty($datasrc[1])) && (empty($src) || empty($src[1]) || $src[1] = 'about:blank')) {
				return;
			}

			if(!empty($datasrc) && !empty($datasrc[1])) {
				if(!empty($src) && !empty($src[1]) && strpos($src[1], '//') !== false) {
					//$url = $src[1];
					$replaceframe = str_replace($datasrc[0], '', $iframe);
				}
				elseif(!empty($src)) {
					//$url = $datasrc[1];
					$replaceframe = str_replace($src[0], '', $iframe);
					$replaceframe = str_replace('data-src', 'src', $replaceframe);
				}
				else {
					$replaceframe = str_replace('data-src', 'src', $iframe);
				}
			}
			$replaceframe = str_replace('src=', 'data-src=', $replaceframe);
		}
		else {
			$replaceframe = str_replace('src=', 'data-src=', $iframe);
		}

		if(strpos($iframe, 'class=') === false) {
			$replaceframe = str_replace('<iframe', '<iframe class="lazyload"', $replaceframe);
		}
		else {
			preg_match('#class=[\'"]?([^\'"]*)[\'"]?#', $iframe, $class);
			if(!empty($class) && !empty($class[1])) {
				$classes = explode(' ', $class[1]);
				$classes[] = 'lazyload';
				$replaceframe = str_replace($class[0], 'class="' . implode(' ', $classes) . '"', $replaceframe);
			}
		}

		$buffer = str_replace($iframe, $replaceframe, $buffer);
	}


	/**
	 * lazyload_script_videos function.
	 *
	 * @access public
	 * @param mixed &$buffer
	 * @return void
	 */
	function lazyload_script_videos(&$buffer) {
		if( WordKeeper_Speed_Helpers::is_optimization_disabled($this->settings) ) {
			return $buffer;
		}
		//global $wordkeeper;

		if(strpos($buffer, 'fast.wistia.net') !== false) {
			$buffer = preg_replace('#<script[^>]*src="//fast\.wistia\.net/assets/external/E-v1\.js" async></script>#', '', $buffer);
			$buffer = str_replace('<script src="//fast.wistia.com/assets/external/E-v1.js" async></script>', '', $buffer);
			$buffer = preg_replace('#<div class="wistia_embed wistia_async_[^"]*" style="[^"]*">&nbsp;</div>#', '', $buffer);

			preg_match_all('#<script [^>]*src="//fast\.wistia\.com/embed/medias/([^>])*\.jsonp"[^>]*>*</script>#', $buffer, $wistias);
			foreach($wistias[0] as $wistia) {
				preg_match('#src=[\'"]([^\'"]*)[\'"]#', $wistia, $src);
				$src = explode('?', $src[1]);
				$src = str_replace('//fast.wistia.com/embed/medias/', '', $src[0]);
				$src = str_replace('.jsonp', '', $src);
				$embed = $src;

				if($this->settings['wordkeeper']['lazyload-videos-mode'] == 'default') {
					$template = trim(file_get_contents(dirname(__FILE__) . '/templates/lazyload.video.html'));
				}
				else {
					$template = trim(file_get_contents(dirname(__FILE__) . '/templates/lightbox.video.html'));
				}
				$replace = str_replace('{type}', 'wistia', $template);
				$replace = str_replace('{embed}', $embed, $replace);
				$replace = str_replace('{video}', 'javascript:;', $replace);
				$replace = str_replace('data-fancybox', 'data-fancybox="iframe" data-type="iframe" data-src="//fast.wistia.net/embed/iframe/' . $embed . '?autoplay=1"', $replace);

				$buffer = str_replace($wistia, $replace, $buffer);
			}
		}

		return $buffer;
	}


	/**
	 * process_buffer_images function.
	 *
	 * @access public
	 * @param mixed &$buffer
	 * @param mixed &$resources
	 * @return void
	 */
	function process_buffer_images(&$buffer, &$resources) {

		if( WordKeeper_Speed_Helpers::is_optimization_disabled($this->settings) ) {
			return $buffer;
		}

		// Skip feature if feature is disabled by overrides
		$feature = apply_filters('wordkeeper_filter_overrides', __FUNCTION__, $buffer);
		if($feature === false) {
			return;
		}

		foreach($resources['images'] as $image) {
			$this->lazyload_image($image, $buffer);
		}
	}


	/**
	 * process_buffer_videos function.
	 *
	 * @access public
	 * @param mixed &$buffer
	 * @param mixed &$resources
	 * @return void
	 */
	function process_buffer_videos(&$buffer, &$resources) {

		if( WordKeeper_Speed_Helpers::is_optimization_disabled($this->settings) ) {
			return $buffer;
		}

		// Skip feature if feature is disabled by overrides
		$feature = apply_filters('wordkeeper_filter_overrides', __FUNCTION__, $buffer);
		if($feature === false) {
			return;
		}

		foreach($resources['videos'] as $video) {
			$this->lazyload_video($video, $buffer);
		}

		return $buffer;
	}


	/**
	 * process_buffer_iframes function.
	 *
	 * @access public
	 * @param mixed &$buffer
	 * @param mixed &$resources
	 * @return void
	 */
	function process_buffer_iframes(&$buffer, &$resources) {

		if( WordKeeper_Speed_Helpers::is_optimization_disabled($this->settings) ) {
			return $buffer;
		}

		// Skip feature if feature is disabled by overrides
		$feature = apply_filters('wordkeeper_filter_overrides', __FUNCTION__, $buffer);
		if($feature === false) {
			return;
		}

		foreach($resources['iframes'] as $frame) {
			if(strpos($buffer, '<noscript>' . $frame) === false) {
				$this->lazyload_iframe($frame, $buffer);
			}
		}
	}


	/**
	 * defer_buffer_scripts function.
	 *
	 * @access public
	 * @param mixed &$buffer
	 * @param mixed &$resources
	 * @return void
	 */
	function defer_buffer_scripts(&$buffer, &$resources) {

		if( WordKeeper_Speed_Helpers::is_optimization_disabled($this->settings) ) {
			return;
		}

		// Skip feature if feature is disabled by overrides
		$feature = apply_filters('wordkeeper_filter_overrides', __FUNCTION__, $buffer);
		if($feature === false) {
			return;
		}

		foreach($resources['scripts'] as $script) {

			// If this script needs to be skipped
			if($this->is_script_partially_excluded($script)){
				$s1 = str_replace('defer=\"defer\"', '', $script);
				$s1 = str_replace('defer=\'defer\'', '', $script);
				$s1 = str_replace('defer', '', $s1);
				$buffer = str_replace($script, $s1, $buffer);
				continue;
			}

			preg_match_all('/<\s?script.*?\s?>/', $script, $begin);

			if ($begin[0][0] == '<script>') {;
				$replace = str_replace('<script>', '<script defer="defer">', $script);
				$buffer = str_replace($script, $replace, $buffer);
			}
			elseif (
				strpos($begin[0][0], 'defer=') === false &&
				strpos($begin[0][0], ' async') === false &&
				strpos($begin[0][0], 'data-skip-defer') === false &&
				preg_match('# data-cfasync=[\'"]*false#', $begin[0][0]) == 0
			) {
				$replace = str_replace('<script ', '<script defer="defer" ', $begin[0]);
				$replace = str_replace($begin[0], $replace, $script);
				$buffer = str_replace($script, $replace, $buffer);
			}
		}
	}

	/**
	 * is_script_partially_excluded function.
	 *
	 * @access public
	 * @param mixed $script
	 * @return bool
	 */
	function is_script_partially_excluded($script) {
		if (
			strpos($script, '.google-analytics.com') === false &&
			strpos($script, 'www.googleadservices.com') === false &&
			strpos($script, 'connect.facebook.net') === false &&
			strpos($script, 'var google_conversion_id') === false &&
			strpos($script, 'var google_conversion_id') === false &&
			(strpos($script, '/jquery.js') === false || strpos($script, '/jquery.json') !== false) &&
			strpos($script, '/jquery.min.js') === false &&
			strpos($script, '/jquery-migrate.js') === false &&
			strpos($script, '/jquery-migrate.min.js') === false &&
			strpos($script, 'try{jQuery.noConflict();}catch(e){};') === false &&
			strpos($script, '/js_composer_front.min.js') === false &&
			strpos($script, '/jquery.nivo.slider.pack.js') === false &&
			strpos($script, '/jquery.themepunch.tools.min.js') === false &&
			strpos($script, '/query-monitor.js') === false &&
			strpos($script, 'formstack.com/forms/js.php') === false &&
			strpos($script, 'typekit') === false &&
			strpos($script, 'thrive') === false &&
			strpos($script, '/jquery.themepunch.revolution.min.js') === false &&
			apply_filters('wordkeeper_partially_excluded_scripts', $script)
			//(strpos($script, 'type=') === false || preg_match('#type=[\'"]text/javascript[\'"]#', $script) === 1)
		){
			return false;
		}
		return true;
	}


	/**
	 * minify function.
	 *
	 * @access public
	 * @param mixed &$buffer
	 * @return void
	 */
	function minify($buffer) {
		//global $wordkeeper;

		if($this->settings['wordkeeper']['minify-js']) {
			preg_match_all('/(?s)<\s?script.*?(?:\/\s?>|<\s?\/\s?script\s?>)/', $buffer, $scripts);
			foreach($scripts[0] as $script) {
				if(strpos($script, '.js') !== false && strpos($script, 'src=') !== false) {
					preg_match('# src=[\'"]([^\'"]*)[\'"]#', $script, $src);
					if(!empty($src) && !empty($src[1])) {
						$this->minify_asset($src[1], $script, $buffer);
					}
				}
			}
		}

		if($this->settings['wordkeeper']['minify-css']) {
			preg_match_all('/(?s)<\s?link.*?(?:\/\s?>|<\s?\/\s?link\s?>)/', $buffer, $links);
			foreach($links[0] as $link) {
				if(strpos($link, '.css') !== false && strpos($link, 'href=') !== false) {
					preg_match('# href=[\'"]([^\'"]*)[\'"]#', $link, $src);
					if(!empty($src) && !empty($src[1])) {
						$this->minify_asset($src[1], $link, $buffer);
					}
				}
			}
		}

		return $buffer;
	}


	/**
	 * minify_asset function.
	 *
	 * @access public
	 * @param mixed $src
	 * @param mixed $asset
	 * @param mixed &$buffer
	 * @return void
	 */
	function minify_asset($src, $asset, &$buffer) {
		$uri = parse_url($src);
		$host = '';
		if(isset($uri['host']) || strpos($host, '.') === false) {
			$host = $uri['host'];
		}

		if(empty($host) || $host == $_SERVER['HTTP_HOST']) {
			if(is_multisite()) {
				$cache_root = '/wp-content/cache/minify/' . get_current_blog_id();
			}
			else {
				$cache_root = '/wp-content/cache/minify';
			}

			$cache_file = rtrim(ABSPATH, '/') . $cache_root . $uri['path'];

			if(!file_exists($cache_file)) {
				$scheme = (is_ssl()) ? 'https' : 'http';
				$minify_url = plugin_dir_url(__FILE__) . 'minify/?f=' . $uri['path'];
				$minified_result = wp_safe_remote_get($minify_url);

				if(200 !== wp_remote_retrieve_response_code($minified_result)) {
					return;
				}

				$content = wp_remote_retrieve_body($minified_result);
				if(!file_exists(dirname($cache_file))) {
					mkdir(dirname($cache_file), 0755, true);
				}
				file_put_contents($cache_file, $content);
			}

			$replace = str_replace($uri['path'], $cache_root . $uri['path'], $asset);
			$cleanpath = str_replace('//', '/', $uri['path']);
			$replace = str_replace($uri['path'], $cleanpath, $replace);
			$buffer = str_replace($asset, $replace, $buffer);
		}
	}


	/**
	 * apply_deferscript function.
	 *
	 * @access public
	 * @param mixed &$buffer
	 * @return void
	 */
	function apply_deferscript($buffer) {

		if( WordKeeper_Speed_Helpers::is_optimization_disabled($this->settings) ) {
			return $buffer;
		}

		//global $wordkeeper;

		// Skip feature if feature is disabled by overrides
		$feature = apply_filters('wordkeeper_filter_overrides', __FUNCTION__, $buffer);
		if($feature === false) {
			return $buffer;
		}

		if($_SERVER['SCRIPT_NAME'] == '/index.php') {
			if(wp_script_is('jquery')) {
				if(strpos($buffer, 'wordkeeper_lazyload_scripts') === false){
					$deferscript = 'if(typeof jQuery != \'undefined\'){(function($){$(window).load(function(e){setTimeout(\'wordkeeper_lazyload_scripts();\',' . $this->settings['wordkeeper']['deferscript-delay'] . ');});})(jQuery);function wordkeeper_lazyload_scripts(){jQuery(function(){if(typeof $==\'undefined\'){$=jQuery;} $(\'[type="text/deferscript"]\').each(function(index,item){$item=$(item);if(typeof $item.attr(\'src\')!=\'undefined\'){$.getScript($item.attr(\'src\'));}
			else{var replace=item.outerHTML.replace(\'deferscript\',\'javascript\');$item.replaceWith(replace);}});});}}';
					$buffer = str_replace('</body>', '<script defer="defer">' . $deferscript . "</script>\n</body>", $buffer);
				}
			}
		}

		return $buffer;
	}


	/**
	 * strip_duplicate_assets function.
	 *
	 * @access public
	 * @param mixed &$buffer
	 * @return void
	 */
	function strip_duplicate_assets(&$buffer) {
		$assets = array();

		preg_match_all('/(?s)<\s?script.*?(?:\/\s?>|<\s?\/\s?script\s?>)/', $buffer, $scripts);
		preg_match_all('/(?s)<\s?link.*?(?:\/\s?>|<\s?\/\s?link\s?>)/', $buffer, $links);

		foreach($scripts[0] as $script) {
			preg_match('# src=[\'"]([^\'"]*)[\'"]#', $script, $src);

			if(!empty($src)) {
				$md5 = md5($src[1]);

				if(isset($assets[$md5])) {
					$buffer = str_replace($script, '', $buffer);
				}
				$assets[$md5] = true;
			}
		}

		foreach($links[0] as $link) {
			preg_match('# href=[\'"]([^\'"]*)[\'"]#', $link, $href);

			if(!empty($href)) {
				$md5 = md5($href[1]);

				if(isset($assets[$md5])) {
					$buffer = str_replace($link, '', $buffer);
				}
				$assets[$md5] = true;
			}
		}

		unset($assets);

		return $buffer;
	}


	/**
	 * strip_session_cookie function.
	 *
	 * Strip PHPSESSID from cookies if present in headers
	 *
	 * @access public
	 * @return void
	 */
	function strip_session_cookie() {
		$cookies = array();
		$session = false;
		if (!headers_sent()) {
			foreach (headers_list() as $header) {
				if (stripos($header, 'Set-Cookie:') !== false) {
					if(strpos($header, 'PHPSESSID') !== false) {
						$session = true;
					}
					else {
						$cookies[] = $header;
					}
				}
			}
		}

		if($session) {
			header_remove('Set-Cookie');
			if(!empty($cookies)) {
				foreach($cookies as $cookie) {
					header($cookie);
				}
			}
		}
	}


	/**
	 * strip_comments function.
	 *
	 * @access public
	 * @param mixed &$buffer
	 * @return void
	 */
	function strip_comments(&$buffer) {
		$buffer = preg_replace('/<!--(.*)-->/Uis', '', $buffer);

		return $buffer;
	}


	/**
	 * remove_google_maps function.
	 *
	 * @access public
	 * @param mixed &$buffer
	 * @return void
	 */
	function remove_google_maps(&$buffer) {
		$buffer = preg_replace('#<script[^>]*src=[\'"]*(?:http[s]*:)*//maps\.google(?:apis)*\.com/maps/api/js[^>]*>[\s\t\r\n]*</script>#', '', $buffer);
		$buffer = preg_replace('#<link[^>]*rel=[\'"]*dns-prefetch[\'"]*[^>]href=[\'"]*(?:http[s]*:)*//maps\.google(?:apis)*\.com[^>]*/>#', '', $buffer);
	}


	/**
	 * concatenate_google_fonts function.
	 *
	 * @access public
	 * @param mixed &$buffer
	 * @return void
	 */
	function concatenate_google_fonts(&$buffer) {
		// Get all Google Fonts CSS files.
		//$buffer_without_comments = preg_replace('/<!--(.*)-->/Uis', '', $buffer);
		preg_match_all('/<link(?:\s.+)?\shref\s*=\s*(\'|")((?!\1).+fonts\.googleapis\.com(?!\1).+)\1(?:\s.*)?>/iU', $buffer, $matches);

		$i = 0;
		$fonts   = array();
		$subsets = array();

		if(!$matches[2]) {
			return $buffer;
		}

		foreach($matches[2] as $index => $font) {
			if(!preg_match( '/rel=["\']dns-prefetch["\']/', $matches[0][$i])) {
				// Get fonts name.
				$font = str_replace(array('%7C', '%7c'), '|', $font);
				$font = explode('family=', $font);
				$font = (isset($font[1])) ? explode('&', $font[1]) : array();

				// Add font to the collection.
				$fonts = array_merge($fonts, explode('|', reset($font)));

				// Add subset to collection.
				$subset = (is_array($font)) ? end($font) : '';
				if(false !== strpos($subset, 'subset=')) {
					$subset  = explode('subset=', $subset);
					$subsets = array_merge($subsets, explode(',', $subset[1]));
				}

				// Delete the Google Fonts tag.
				if($index == 0) {
					$buffer = str_replace($matches[0][$i], '{google-fonts}', $buffer);
				}
				else {
					$buffer = str_replace($matches[0][$i], '', $buffer);
				}
			}

			$i++;
		}

		// Concatenate fonts tag.
		$subsets = ($subsets) ? '&subset=' . implode(',', array_filter(array_unique($subsets))) : '';
		$fonts   = trim(implode('|' , array_filter(array_unique($fonts))), '|');
		$fonts  = str_replace('|', '%7C', $fonts);

		if(!empty($fonts)) {
			$fonts   = '<link rel="stylesheet" href="//fonts.googleapis.com/css?family=' . $fonts . $subsets . '" />';
			$buffer = str_replace('{google-fonts}', $fonts, $buffer);
		}
		else{
			$buffer = str_replace('{google-fonts}', '', $buffer);
		}

		return $buffer;
	}


	/**
	 * remove_query_strings function.
	 *
	 * @access public
	 * @param mixed $src
	 * @return void
	 */
	function remove_query_strings($src) {

		if (!is_admin()) {
			$siteurl = get_site_url();
			$siteurl = parse_url($siteurl);
			$host = $siteurl['host'];

			if (strpos($src, '.php') === false && strpos($src, $host) !== false) {
				$parts = explode('?', $src);
				$path = $parts[0];
				return $path;
			}
			else {
				return $src;
			}
		}

		return $src;
	}


	/**
	 * heartbeat_control function.
	 *
	 * @since    1.0.0
	 * @access public
	 * @return void
	 */
	function heartbeat_control() {
		global $pagenow;

		if($pagenow != 'post.php' && $pagenow != 'post-new.php') {
			wp_deregister_script('heartbeat');
		}
	}


	/**
	 * heartbeat_frequency function.
	 *
	 * @since    1.0.0
	 * @access public
	 * @param mixed $settings
	 * @return void
	 */
	function heartbeat_frequency($settings) {
		$settings['interval'] = 60;
		return $settings;
	}


	/**
	 * apply_resource_hints function.
	 *
	 * @access public
	 * @param mixed &$buffer
	 * @return void
	 */
	function apply_resource_hints($buffer) {
		preg_match_all('/(?s)<\s?script.*?(?:\/\s?>|<\s?\/\s?script\s?>)/', $buffer, $scripts);
		preg_match_all('/(?s)<\s?link.*?(?:\/\s?>|<\s?\/\s?link\s?>)/', $buffer, $links);

		$type = 'script';
		foreach($scripts[0] as $resource) {
			$src = '';
			if(strpos($resource, 'src') !== false) {
				preg_match('# src=[\'"]*([^\'"]+)[\'"]#', $resource, $src);
				$src = (!empty($src)) ? $src[1] : '';
				$uri = parse_url($src);
				$host = '';
				if(isset($uri['host'])) {
					$host = $uri['host'];
				}

				if(empty($host) || $host == $_SERVER['HTTP_HOST']) {
					$src = ('//' === substr($src, 0, 2)) ? preg_replace('/^\/\/([^\/]*)\//', '/', $src) : preg_replace('/^http(s)?:\/\/[^\/]*/', '', $src);
				}
				$src = trim($src);
				if(!empty($src)) {
					$this->add_resource_hint_header($src, $type);
				}
			}
		}

		$type = 'style';
		foreach($links[0] as $resource) {
			$src = '';
			if(strpos($resource, 'stylesheet') !== false || strpos($resource, '.css') !== false) {
				preg_match('# href=[\'"]*([^\'"]+)[\'"]#', $resource, $src);
				$src = (!empty($src)) ? $src[1] : '';
				$uri = parse_url($src);
				$host = '';
				if(isset($uri['host'])) {
					$host = $uri['host'];
				}

				if(empty($host) || $host == $_SERVER['HTTP_HOST']) {
					$src = ('//' === substr($src, 0, 2)) ? preg_replace('/^\/\/([^\/]*)\//', '/', $src) : preg_replace('/^http(s)?:\/\/[^\/]*/', '', $src);
				}
				$src = trim($src);
				if(!empty($src)) {
					$this->add_resource_hint_header($src, $type);
				}
			}
		}
		return $buffer;
	}


	/**
	 * add_resource_hint_header function.
	 *
	 * @since    1.0.0
	 * @access public
	 * @param mixed $src
	 * @param mixed $type
	 * @return void
	 */
	function add_resource_hint_header($src, $type) {
		static $header_size = 0;

		$url = esc_url($src);
		if(!empty($url)) {
			$nopush = WordKeeper_Speed_Helpers::is_external_url($url) ? '; nopush' : '';
			$header = sprintf(
				'Link: <%s>; rel=preload; as=%s' . $nopush,
				$url,
				$type
			);

			// Ensure that the header is within reasonable bounds
			if(($header_size + strlen($header)) < 4096) {
				$header_size += strlen($header);
				header($header, false);
			}
		}
	}


	/**
	 * remove_wordpress_generator function.
	 *
	 * Disable the WP generator tag
	 *
	 * @access public
	 * @return void
	 */
	function remove_wordpress_generator() {
		return '';
	}


	/**
	 * remove_revslider_generator function.
	 *
	 * Disable the Revolution Slider generator tag
	 *
	 * @access public
	 * @return void
	 */
	function remove_revslider_generator() {
		return '';
	}


	/**
	 * wordkeeper_remove_visual_composer_generator function.
	 *
	 * Disable the Visual Composer generator tag
	 *
	 * @access public
	 * @return void
	 */
	function remove_visual_composer_generator() {
		if(function_exists('visual_composer')) {
			remove_action('wp_head', array(visual_composer(), 'addMetaData'));
		}
	}


	/**
	 * remove_woocommerce_generator function.
	 *
	 * Disable the WooCommerce generator tag
	 *
	 * @access public
	 * @return void
	 */
	function remove_woocommerce_generator() {
		remove_action('wp_head','wc_generator_tag');
	}


	/**
	 * disable_emojis function.
	 *
	 * Disable Emojis
	 *
	 * @access public
	 * @return void
	 */
	function disable_emojis() {
		remove_action('wp_head', 'print_emoji_detection_script', 7);
		remove_action('admin_print_scripts', 'print_emoji_detection_script');
		remove_action('wp_print_styles', 'print_emoji_styles');
		remove_action('admin_print_styles', 'print_emoji_styles');
		remove_filter('the_content_feed', 'wp_staticize_emoji');
		remove_filter('comment_text_rss', 'wp_staticize_emoji');
		remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
	}


	/**
	 * disable_tinymce_emojis function.
	 *
	 * Disable Emojis in TinyMCE
	 *
	 * @access public
	 * @param mixed $plugins
	 * @return void
	 */
	function disable_tinymce_emojis($plugins) {
		if(is_array($plugins)) {
			return array_diff($plugins, array('wpemoji'));
		}
		else {
			return array();
		}
	}


	/**
	 * disable_emojis_dns_prefetch function.
	 *
	 * Disable Emoji DNS prefetch
	 *
	 * @access public
	 * @param mixed $urls
	 * @param mixed $relation_type
	 * @return void
	 */
	function disable_emojis_dns_prefetch($urls, $relation_type) {
		if('dns-prefetch' == $relation_type) {
			/** This filter is documented in wp-includes/formatting.php */
			$emoji_svg_url = apply_filters('emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/');

			$urls = array_diff($urls, array($emoji_svg_url));
		}

		return $urls;
	}


	/**
	 * disable_embeds function.
	 *
	 * Disable oEmbed support
	 *
	 * @access public
	 * @return void
	 */
	function disable_embeds() {

		// Remove the REST API endpoint.
		remove_action('rest_api_init', 'wp_oembed_register_route');

		// Turn off oEmbed auto discovery.
		add_filter('embed_oembed_discover', '__return_false');

		// Don't filter oEmbed results.
		remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);

		// Remove oEmbed discovery links.
		remove_action('wp_head', 'wp_oembed_add_discovery_links');

		// Remove oEmbed-specific JavaScript from the front-end and back-end.
		remove_action('wp_head', 'wp_oembed_add_host_js');

		// Remove filter of the oEmbed result before any HTTP requests are made.
		remove_filter('pre_oembed_result', 'wp_filter_pre_oembed_result', 10);
	}


	/**
	 * disable_tinymce_embeds function.
	 *
	 * Disable TinyMCE oEmbed support
	 *
	 * @access public
	 * @param mixed $plugins
	 * @return void
	 */
	function disable_tinymce_embeds($plugins) {
		return array_diff($plugins, array('wpembed'));
	}


	/**
	 * Disable oEmbed rewrites
	 *
	 * @since    1.0.0
	 */
	function disable_embed_rewrites($rules) {
		foreach($rules as $rule => $rewrite) {
			if(false !== strpos($rewrite, 'embed=true')) {
				unset($rules[$rule]);
			}
		}

		return $rules;
	}


	/**
	 * Sets the 'no_found_rows' param to true.
	 *
	 * Replace SQL_CALC_ROWS with no_found_rows to prevent slow queries
	 *
	 * @param  WP_Query $wp_query The WP_Query instance. Passed by reference.
	 * @return void
	 */
	function db_set_no_found_rows(\WP_Query $wp_query) {
		$wp_query->set('no_found_rows', true);
	}


	/**
	 *
	 * Handle pagination correctly when replacing count queries with COUNT * instead of CALC_FOUND_ROWS
	 *
	 * @param array    $clauses  Array of clauses that make up the SQL query.
	 * @param WP_Query $wp_query The WP_Query instance. Passed by reference.
	 * @return array
	 */
	function db_set_found_posts($clauses, \WP_Query $wp_query) {
		// Ignore this for queries that don't involve multiple posts
		if ($wp_query->is_singular()) {
			return $clauses;
		}

		global $wpdb;

		// Determine if there are any query clauses to account for
		$where = isset($clauses[ 'where' ]) ? $clauses[ 'where' ] : '';
		$join = isset($clauses[ 'join' ]) ? $clauses[ 'join' ] : '';
		$distinct = isset($clauses[ 'distinct' ]) ? $clauses[ 'distinct' ] : '';

		// Replace SQL_CALC_ROWS with COUNT *
		$wp_query->found_posts = $wpdb->get_var("SELECT $distinct COUNT(*) FROM {$wpdb->posts} $join WHERE 1=1 $where");

		// Determine posts per page
		$posts_per_page = (!empty($wp_query->query_vars['posts_per_page']) ? absint($wp_query->query_vars['posts_per_page']) : absint(get_option('posts_per_page')));

		// Set the number of max pages
		$wp_query->max_num_pages = ceil($wp_query->found_posts / $posts_per_page);

		// Return $clauses so query can run.
		return $clauses;
	}

	/**
	 * dequeue_woocommerce_cart_fragments function.
	 *
	 * Remove WooCommerce get_refreshed_fragments calls from occurring by default
	 *
	 * @access public
	 * @return void
	 */
	function dequeue_woocommerce_cart_fragments() {
		wp_dequeue_script('wc-cart-fragments');
	}

	/**
	 * custom_defer function.
	 *
	 * Add custom logic to find and convert the script tags to deferscript
	 *
	 * @access public
	 * @param mixed $buffer
	 * @return mixed $buffer
	 */
	function custom_defer($buffer) {
		if($this->settings['wordkeeper']['use-deferscript']) {
			preg_match_all('/(?s)<\s?script.*?(?:\/\s?>|<\s?\/\s?script\s?>)/', $buffer, $scripts);

			foreach ($scripts[0] as $script) {
				if(
					strpos($script, 'pattern1') !== false ||
					strpos($script, 'pattern2') !== false
				) {
					preg_match_all('/<\s?script.*?\s?>/', $script, $begin);
					if(strpos($begin[0], ' type=') !== false && strpos($begin[0], 'text/javascript') !== false) {
						$replace = str_replace('text/javascript', 'text/deferscript', $begin[0]);
						$replace = str_replace($begin[0], $replace, $script);
						$buffer = str_replace($script, $replace, $buffer);
					}
					else {
						$replace = str_replace('<script', '<script type="text/deferscript"', $script);
						$buffer = str_replace($script, $replace, $buffer);
					}
				}
			}
		}

		return $buffer;
	}

	/**
	 * cache_widget_output
	 * @param array     $instance The current widget instance's settings.
	 * @param WP_Widget $widget     The current widget instance.
	 * @param array     $args     An array of default widget arguments.
	 * @return mixed array|boolean
	 */
	function cache_widget_output($instance, $widget, $args) {
		if ( false === $instance ) {
			return $instance;
		}

		//skip cache on preview
		if ( $widget->is_preview() ){
			return $instance;
		}

		//check if we need to cache this widget?
		if(isset($instance['wordkeeper_widget_cache']) && $instance['wordkeeper_widget_cache'] == true) {
			return $instance;
		}

		$url_specific_cache = isset($instance['wordkeeper_widget_cache_url']) && $instance['wordkeeper_widget_cache_url'] == true;
		//simple timer to clock the widget rendering
		$timer_start = microtime(true);

		$key = WordKeeper_Speed_Widget_Cache::generate_cache_key($instance,$widget, $url_specific_cache);

		//get the "cached version of the widget"
		if ( false === ( $cached_widget = WordKeeper_Speed_Widget_Cache::get_cache( $key ) ) ){
			// It wasn't there, so render the widget and save it in cache
			// start a buffer to capture the widget output
			ob_start();
			//this renders the widget
			$widget->widget( $args, $instance );
			//get rendered widget from buffer
			$cached_widget = ob_get_contents();
			ob_end_clean();
			//save/cache the widget output
			WordKeeper_Speed_Widget_Cache::set_cache($key, $cached_widget, $this->settings['wordkeeper']['cache-duration']);
		}

		echo $cached_widget;
		echo '<!-- From widget cache in ' . number_format( microtime(true) - $timer_start, 5 ) . ' seconds -->';

		//after the widget was rendered and printed we return false to short-circuit the normal display of the widget
		return false;
	}

	/**
	 * show_widget_cache_options
	 * this method displays a checkbox in the widget panel
	 *
	 * @param WP_Widget $t     The widget instance, passed by reference.
	 * @param null      $return   Return null if new fields are added.
	 * @param array     $instance An array of the widget's settings.
	 *
	 */
	function show_widget_cache_options($t,$return,$instance) {
		$instance = wp_parse_args(
			(array) $instance,
			array(
				'title' => '',
				'text' => '',
				'wordkeeper_widget_cache' => null,
				'wordkeeper_widget_cache_url' => null
			)
		);

		$disable_checkbox = isset($instance['wordkeeper_widget_cache']) && $instance['wordkeeper_widget_cache'] == true ? 'disabled' : '';

		if ( !isset($instance['wordkeeper_widget_cache']) )
			$instance['wordkeeper_widget_cache'] = null;

		if ( !isset($instance['wordkeeper_widget_cache_url']) )
			$instance['wordkeeper_widget_cache_url'] = null;
?>
        <p>
            <input id="<?php echo $t->get_field_id('wordkeeper_widget_cache'); ?>" class="wordkeeper_widget_cache_checkbox" name="<?php echo $t->get_field_name('wordkeeper_widget_cache'); ?>" type="checkbox" <?php checked(isset($instance['wordkeeper_widget_cache']) ? $instance['wordkeeper_widget_cache'] : 0); ?> />
            <label for="<?php echo $t->get_field_id('wordkeeper_widget_cache'); ?>"><?php _e('Don\'t Cache This Widget'); ?></label>
        </p>

		<p>
            <input id="<?php echo $t->get_field_id('wordkeeper_widget_cache_url'); ?>" name="<?php echo $t->get_field_name('wordkeeper_widget_cache_url'); ?>" type="checkbox" <?php checked(isset($instance['wordkeeper_widget_cache_url']) ? $instance['wordkeeper_widget_cache_url'] : 0);  echo $disable_checkbox; ?> />
            <label for="<?php echo $t->get_field_id('wordkeeper_widget_cache_url'); ?>"><?php _e('Cache Widget Separately for Each URL'); ?></label>
        </p>
        <?php
	}

	/**
	 * widget_update_callback
	 * @param array     $instance     The current widget instance's settings.
	 * @param array     $new_instance Array of new widget settings.
	 * @param array     $old_instance Array of old widget settings.
	 * @return array    $instance
	 */
	function widget_update_callback($instance, $new_instance, $old_instance, $widget) {
		// Purge the cache upon updating
		$key = WordKeeper_Speed_Widget_Cache::generate_cache_key($old_instance,$widget);
		WordKeeper_Speed_Widget_Cache::delete_cache($widget->id);

		//save the checkbox if its set
		$instance['wordkeeper_widget_cache'] = isset($new_instance['wordkeeper_widget_cache']);
		$instance['wordkeeper_widget_cache_url'] = isset($new_instance['wordkeeper_widget_cache_url']);
		return $instance;
	}

	/**
	 * delete_widget
	 * @param array     $widget_id  The current widget's id
	 * @param array     $sidebar_id  sidebar_id where the widget was deleted from
	 * @param array     $id_base  ID base for the widget
	 * @return void
	 */
	function delete_widget($widget_id, $sidebar_id, $id_base) {
		WordKeeper_Speed_Widget_Cache::delete_cache($widget_id);
	}

	/**
	 * admin_bar_purge_cache
	 * This method displays options to purge cache from admin menu bar on the top
	 *
	 * @return void
	 */
	function admin_bar_purge_cache() {
		global $wp_admin_bar;
		global $wp;

		$purge_all_url = home_url( $wp->request );
		$widget_purge_url = home_url( $wp->request );
		$menu_purge_url = home_url( $wp->request );
		$minify_purge_url = home_url( $wp->request );
		$fragment_purge_url = home_url( $wp->request );

		if(strpos($purge_all_url, '?') !== false) {
			$purge_all_url .= '&wordkeeper_purge_all_cache=1';
			$widget_purge_url .= '&wordkeeper_purge_widget_cache=1';
			$menu_purge_url .= '&wordkeeper_purge_menu_cache=1';
			$minify_purge_url .= '&wordkeeper_purge_minify_cache=1';
		}
		else {
			$purge_all_url .= '?wordkeeper_purge_all_cache=1';
			$widget_purge_url .= '?wordkeeper_purge_widget_cache=1';
			$menu_purge_url .= '?wordkeeper_purge_menu_cache=1';
			$minify_purge_url .= '?wordkeeper_purge_minify_cache=1';
			$fragment_purge_url .= '?wordkeeper_purge_fragment_cache=1';
		}

		if($this->settings['wordkeeper']['partials']) {
			$wp_admin_bar->add_menu( array(
					'parent' => false, // use 'false' for a root menu, or pass the ID of the parent menu
					'id' => 'wordkeeper_purge_caches', // link ID, defaults to a sanitized title value
					'title' => __('Clear Partial Caches'), // link title
					'href' => '#', // name of file
					'meta' => false // array of any of the following options: array( 'html' => '', 'class' => '', 'onclick' => '', target => '', title => '' );
				));

			$wp_admin_bar->add_menu( array(
					'parent' => 'wordkeeper_purge_caches', // use 'false' for a root menu, or pass the ID of the parent menu
					'id' => 'wordkeeper_purge_all_caches', // link ID, defaults to a sanitized title value
					'title' => __('Clear All Partial Caches'), // link title
					'href' => $purge_all_url, // name of file
					'meta' => false // array of any of the following options: array( 'html' => '', 'class' => '', 'onclick' => '', target => '', title => '' );
				));

			if($this->settings['wordkeeper']['cache-widgets'] && current_user_can('edit_theme_options')) {
				$wp_admin_bar->add_menu( array(
						'parent' => 'wordkeeper_purge_caches', // use 'false' for a root menu, or pass the ID of the parent menu
						'id' => 'wordkeeper_purge_widget_caches', // link ID, defaults to a sanitized title value
						'title' => __('Clear Widget Caches'), // link title
						'href' => $widget_purge_url, // name of file
						'meta' => false // array of any of the following options: array( 'html' => '', 'class' => '', 'onclick' => '', target => '', title => '' );
					));
			}

			if($this->settings['wordkeeper']['cache-menus'] && current_user_can('edit_theme_options')) {
				$wp_admin_bar->add_menu( array(
						'parent' => 'wordkeeper_purge_caches', // use 'false' for a root menu, or pass the ID of the parent menu
						'id' => 'wordkeeper_purge_menu_caches', // link ID, defaults to a sanitized title value
						'title' => __('Clear Menu Caches'), // link title
						'href' => $menu_purge_url, // name of file
						'meta' => false // array of any of the following options: array( 'html' => '', 'class' => '', 'onclick' => '', target => '', title => '' );
					));
			}

			if(($this->settings['wordkeeper']['minify-js'] || $this->settings['wordkeeper']['minify-css']) && current_user_can('edit_theme_options')) {
				$wp_admin_bar->add_menu( array(
						'parent' => 'wordkeeper_purge_caches', // use 'false' for a root menu, or pass the ID of the parent menu
						'id' => 'wordkeeper_purge_minify_caches', // link ID, defaults to a sanitized title value
						'title' => __('Clear Minify Caches'), // link title
						'href' => $minify_purge_url, // name of file
						'meta' => false // array of any of the following options: array( 'html' => '', 'class' => '', 'onclick' => '', target => '', title => '' );
					));
			}

			if($this->settings['wordkeeper']['cache-fragments'] && current_user_can('edit_theme_options')) {
				$wp_admin_bar->add_menu( array(
						'parent' => 'wordkeeper_purge_caches', // use 'false' for a root menu, or pass the ID of the parent menu
						'id' => 'wordkeeper_purge_fragment_caches', // link ID, defaults to a sanitized title value
						'title' => __('Clear Fragment Caches'), // link title
						'href' => $fragment_purge_url, // name of file
						'meta' => false // array of any of the following options: array( 'html' => '', 'class' => '', 'onclick' => '', target => '', title => '' );
					));
			}
		}
	}

	/**
	 * post_processing_after_page_loaded
	 * This method is fired after the page is loaded and is used to check if purge cache action was requested.
	 *
	 * @return void
	 */
	function post_processing_after_page_loaded() {
		if(!empty($_GET)){
			$_GET = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
		}
		$wordkeeper_purge_all_cache = isset($_GET['wordkeeper_purge_all_cache']);
		$wordkeeper_purge_widget_cache = isset($_GET['wordkeeper_purge_widget_cache']);
		$wordkeeper_purge_menu_cache = isset($_GET['wordkeeper_purge_menu_cache']);
		$wordkeeper_purge_minify_cache = isset($_GET['wordkeeper_purge_minify_cache']);
		$wordkeeper_purge_fragment_cache = isset($_GET['wordkeeper_purge_fragment_cache']);

		if(current_user_can('edit_theme_options') && $wordkeeper_purge_widget_cache){
			// Purge cache here.
			WordKeeper_Speed_Widget_Cache::purge_all();
		}
		elseif(current_user_can('edit_theme_options') && $wordkeeper_purge_menu_cache){
			// Purge cache here.
			WordKeeper_Speed_Menu_Cache::purge_all();
		}
		elseif(current_user_can('edit_theme_options') && $wordkeeper_purge_minify_cache){
			// Purge cache here.
			WordKeeper_Speed_Helpers::purge_minify_cache();
		}
		elseif(current_user_can('edit_theme_options') && $wordkeeper_purge_fragment_cache){
			// Purge cache here.
			WordKeeper_Speed_Fragment_Cache::purge_all($this->settings['wordkeeper']['cache-folder']);
		}
		elseif(current_user_can('edit_theme_options') && $wordkeeper_purge_all_cache){
			// Purge cache here.
			WordKeeper_Speed_Widget_Cache::purge_all();
			WordKeeper_Speed_Menu_Cache::purge_all();
			WordKeeper_Speed_Fragment_Cache::purge_all($this->settings['wordkeeper']['cache-folder']);
		}
	}

	/**
	 * wp_nav_menu
	 * store menu in cache
	 * @param  string $nav      The HTML content for the navigation menu.
	 * @param  object $args     An object containing wp_nav_menu() arguments
	 * @return string           The HTML content for the navigation menu.
	 */
	function wp_nav_menu( $nav, $args ) {
		$key = WordKeeper_Speed_Menu_Cache::generate_cache_key($args);
		$cached_menu = WordKeeper_Speed_Menu_Cache::get_cache( $key, $args );

		if (false !== $cached_menu){
			return $cached_menu;
		}

		WordKeeper_Speed_Menu_Cache::set_cache($key, $nav, $this->settings['wordkeeper']['cache-duration']);

		return $nav;
	}

	/**
	 * wp_update_nav_menu
	 * refresh time on update to force refresh of cache
	 * @param  int $menu_id
	 * @return void
	 */
	function wp_update_nav_menu($menu_id) {
		$locations = array_flip(get_nav_menu_locations());

		if (isset($locations[$menu_id])) {
			$menu = wp_get_nav_menu_object($menu_id);
			$slug = $menu->slug;

			WordKeeper_Speed_Menu_Cache::delete_menu_caches($slug);
		}
	}

	/**
	 * permalinks_updated
	 * forcibly clear widget/menu caches when permalinks are updated
	 * @param  int $menu_id
	 * @return void
	 */
	function permalinks_updated() {
		if($this->settings['wordkeeper']['cache-widgets']) {
			WordKeeper_Speed_Widget_Cache::purge_all();
		}

		if($this->settings['wordkeeper']['cache-menus']) {
			WordKeeper_Speed_Menu_Cache::purge_all();
		}

		if($this->settings['wordkeeper']['cache-fragments']) {
			WordKeeper_Speed_Fragment_Cache::purge_all($this->settings['wordkeeper']['cache-folder']);
		}
	}

	/**
	 * activated_plugin
	 * Runs whenever a plugin was activated
	 * @param  mixed $plugin
	 * @param  bool $network_activation
	 * @return void
	 */
	function activated_plugin($plugin, $network_activation){
		WordKeeper_Speed_Helpers::purge_minify_cache();

		if($this->settings['wordkeeper']['cache-widgets']) {
			WordKeeper_Speed_Widget_Cache::purge_all();
		}

		if($this->settings['wordkeeper']['cache-menus']) {
			WordKeeper_Speed_Menu_Cache::purge_all();
		}

		if($this->settings['wordkeeper']['cache-fragments']) {
			WordKeeper_Speed_Fragment_Cache::purge_all($this->settings['wordkeeper']['cache-folder']);
		}
	}

	/**
	 * deactivated_plugin
	 * Runs whenever a plugin was deactivated
	 * @param  mixed $plugin
	 * @param  bool $network_activation
	 * @return void
	 */
	function deactivated_plugin($plugin, $network_activation){
		WordKeeper_Speed_Helpers::purge_minify_cache();

		if($this->settings['wordkeeper']['cache-widgets']) {
			WordKeeper_Speed_Widget_Cache::purge_all();
		}

		if($this->settings['wordkeeper']['cache-menus']) {
			WordKeeper_Speed_Menu_Cache::purge_all();
		}

		if($this->settings['wordkeeper']['cache-fragments']) {
			WordKeeper_Speed_Fragment_Cache::purge_all($this->settings['wordkeeper']['cache-folder']);
		}
	}

	/**
	 * activated_plugin
	 * Runs whenever a plugin, theme or core download process was completed
	 * @param  obj  $upgrader_object
	 * @param  array $options
	 * @return void
	 */
	function upgrader_process_complete($upgrader_object, $options){
		if($options['type'] == 'plugin' || $options['type'] == 'theme' || $options['type'] == 'core'){
			WordKeeper_Speed_Helpers::purge_minify_cache();
		}

		if($this->settings['wordkeeper']['cache-widgets']) {
			WordKeeper_Speed_Widget_Cache::purge_all();
		}

		if($this->settings['wordkeeper']['cache-menus']) {
			WordKeeper_Speed_Menu_Cache::purge_all();
		}

		if($this->settings['wordkeeper']['cache-fragments']) {
			WordKeeper_Speed_Fragment_Cache::purge_all($this->settings['wordkeeper']['cache-folder']);
		}
	}

	/**
	 * Deletes all transients that have expired
	 *
	 * @access public
	 * @param bool $safemode
	 * @return void
	 */
	function purge_transients($safemode = false) {

		global $wpdb;
		/**
		 * If safemode is ON just use the default WordPress get_transient() function
		 * to delete the expired transients
		 */

		if ( $safemode ) {
			foreach( $transients as $transient ) {
				get_transient($transient);
			}
		} else {

			$sql = "
				DELETE
					a, b
				FROM
					{$wpdb->options} a, {$wpdb->options} b
				WHERE
					a.option_name LIKE '%_transient_%' AND
					a.option_name NOT LIKE '%_transient_timeout_%' AND
					b.option_name = CONCAT(
						'_transient_timeout_',
						SUBSTRING(
							a.option_name,
							CHAR_LENGTH('_transient_') + 1
						)
					)
				AND b.option_value < UNIX_TIMESTAMP()
			";
			$wpdb->query( $sql );

			if ( is_multisite() && is_main_network() ) {
				$sql = "
					DELETE
						a, b
					FROM
						{$wpdb->sitemeta} a, {$wpdb->sitemeta} b
					WHERE
						a.meta_key LIKE '_site_transient_%' AND
						a.meta_key NOT LIKE '_site_transient_timeout_%' AND
						b.meta_key = CONCAT(
							'_site_transient_timeout_',
							SUBSTRING(
								a.meta_key,
								CHAR_LENGTH('_site_transient_') + 1
							)
						)
					AND b.meta_value < UNIX_TIMESTAMP()
				";

				$wpdb->query( $sql );
			}
		}
	}

	/**
	 * filter_woocommerce_delete_version_transients_limit function
	 *
	 * @access public
	 * @param mixed $int
	 * @return int
	 */
	function filter_woocommerce_delete_version_transients_limit( $int ) {
		return 0;
	}

	/**
	 * woocommerce_defer_transactional_emails function
	 *
	 * @access public
	 * @return boolean
	 */
	function woocommerce_defer_transactional_emails(){
		return true;
	}
}

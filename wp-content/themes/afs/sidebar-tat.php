<?php
	//Social settings
	$social_title = get_field("social_title") ?: get_the_title();
	$social_description = get_field("social_description") ?: '';
	$social_url = get_field("social_url") ?: get_the_permalink();
	$social_media = get_field("social_photo") ?: $post_header;

	$social_categories = get_field("social_categories");

?>

<ul class="social-actions">
	<li class="social-pinterest">
		<a 	data-pin-do="buttonBookmark"
			data-pin-custom="true"
			data-pin-media="<?php echo $post_header['sizes']['hero-xs'] ?>"
			href="https://www.pinterest.com/pin/create/button/">
			<i class="fa fa-pinterest" aria-hidden="true"></i>
		</a>
		<script async defer src="//assets.pinterest.com/js/pinit.js"></script>
	</li>
	<li class="social-houzz">
		<a class="houzz-share-button"
			data-url="<?php echo $social_url ?>"
			data-hzid="40238"
			data-title="<?php echo $social_title ?>"
			data-img="<?php echo $social_media ?>"
			data-desc="<?php echo $social_description ?>"
			data-showcount="0"
			href="https://www.houzz.com">
			<i class="fa fa-houzz" aria-hidden="true"></i>
		</a>
		<script>(function(d,s,id){if(!d.getElementById(id)){var js=d.createElement(s);js.id=id;js.async=true;js.src="//platform.houzz.com/js/widgets.js?"+(new Date().getTime());var ss=d.getElementsByTagName(s)[0];ss.parentNode.insertBefore(js,ss);}})(document,"script","houzzwidget-js");</script>
	</li>
	<li class="social-fb">
		<div class="fb-share-button" data-href="<?php echo $social_url ?>" data-layout="button" data-size="large" data-mobile-iframe="false"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"><i class="fa fa-facebook" aria-hidden="true"></i></a></div>
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
	</li>
	<li class="social-mailto">
		<a href="mailto:?subject=America's Floor Source - <?php echo $social_title ?>
						&body=Check out the latest from America's Floor Source (<?php echo $social_url ?>)">
			<i class="fa fa-envelope" aria-hidden="true"></i>
		</a>
	</li>
</ul>


<?php if ( is_active_sidebar( 'sidebar-tat' ) ) : ?>
	<?php dynamic_sidebar( 'sidebar-tat' ); ?>
<?php endif; ?>

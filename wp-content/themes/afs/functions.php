<?php

include('inc/comments.php');
include('inc/excerpt_read_more.php');
include('inc/gravity_forms.php');
include('inc/misc_functions.php');
include('inc/navs.php');
include('inc/pagination.php');
include('inc/scripts.php');
include('inc/sidebars.php');
include('inc/site_options.php');
include('inc/social_links.php');
include('inc/thumbs.php');
include('inc/walker_nav.php');
include('inc/wp-admin.php');

include('inc/widget.php');
include('inc/hooks.php');

include('inc/woocommerce.php');
// Change sender adress
add_filter( 'woocommerce_email_from_address', function( $from_email, $wc_email ){
    if( $wc_email->id == 'new_order' || $wc_email->id == 'cancelled_order' || $wc_email->id == 'failed_order' )
        $from_email = 'devteam@mobile-marketing.agency';

    return $from_email;
}, 10, 2 );
function edit_upload_types($existing_mimes = array()) {
    // allow .woff
    $existing_mimes['xml'] = 'text/xml';
 
 
    return $existing_mimes;
}
add_filter('upload_mimes', 'edit_upload_types');

function woocommerce_edit_my_account_page() {
    return apply_filters( 'woocommerce_forms_field', array(
        'billing_phone' => array(
            'type'        => 'text',
            'label'       => __( 'Phone Number', ' cloudways' ),
            'placeholder' => __( 'Phone Number', 'cloudways' ),
            'required'    => true,
        ),
        'billing_company' => array(
            'type'        => 'text',
            'label'       => __( 'Company Name (if applicable)', ' cloudways' ),
            'placeholder' => __( 'Company Name (if applicable)', 'cloudways' ),
            'required'    => false,
        ),
    ) );
}
function edit_my_account_page_woocommerce() {
    $fields = woocommerce_edit_my_account_page();
    foreach ( $fields as $key => $field_args ) {
        woocommerce_form_field( $key, $field_args );
    }
}
add_action( 'woocommerce_register_form', 'edit_my_account_page_woocommerce', 15 );

add_action( 'woocommerce_created_customer', 'bbloomer_save_name_fields' );
  
function bbloomer_save_name_fields( $customer_id ) {
    if ( isset( $_POST['billing_phone'] ) ) {
        update_user_meta( $customer_id, 'billing_phone', sanitize_text_field( $_POST['billing_phone'] ) );
     //   update_user_meta( $customer_id, 'first_name', sanitize_text_field($_POST['billing_first_name']) );
    }
    if ( isset( $_POST['billing_company'] ) ) {
        update_user_meta( $customer_id, 'billing_company', sanitize_text_field( $_POST['billing_company'] ) );
     //   update_user_meta( $customer_id, 'last_name', sanitize_text_field($_POST['billing_last_name']) );
    }
  
}

add_action( 'woocommerce_created_customer', 'woocommerce_created_customer_admin_notification' );

function woocommerce_created_customer_admin_notification( $customer_id ) {
  wp_send_new_user_notifications( $customer_id, 'admin' );
}


add_filter( 'wp_new_user_notification_email_admin', 'my_wp_new_user_notification_email_admin', 10, 3 );


// Redefine user notification function

    function my_wp_new_user_notification_email_admin( $wp_new_user_notification_email, $user, $blogname ) {
          
        $user_login = stripslashes($user->user_login);
        $user_email = stripslashes($user->user_email);
        $billing_company = stripslashes($user->billing_company);
        $first_name = stripslashes($user->first_name);
        $last_name = stripslashes($user->last_name);
        $billing_phone = stripslashes($user->billing_phone);
        $blog_name = "America's Floor Source";
  
        $message  = sprintf(__('New user registration on %s:'), $blog_name) . "\r\n\r\n";
        $message .= sprintf(__('Username: %s'), $user_login) . "\r\n";
        $message .= sprintf(__('E-mail: %s'), $user_email) . "\r\n";
        $message .= sprintf(__('First Name: %s'), $first_name) . "\r\n";
        $message .= sprintf(__('Last Name: %s'), $last_name) . "\r\n";
        $message .= sprintf(__('Billing Company: %s'), $billing_company) . "\r\n";
        $message .= sprintf(__('Billing Phone Number: %s'), $billing_phone) . "\r\n";

        $wp_new_user_notification_email['message'] = $message;
        $wp_new_user_notification_email['headers'] = "From: America's Floor Source <devteam@mobile-marketing.agency>";
  
  
        @wp_mail('devteam@mobile-marketing.agency', sprintf(__('[%s] New User Registration'), $blog_name), $message);
       // @wp_mail('devteam.agency@gmail.com', sprintf(__('[%s] New User Registration'), $blog_name), $message);
  
        return $wp_new_user_notification_email;

    }


add_action( 'woocommerce_after_cart_totals', 'wpdesk_cart_disclaimer_text' );
function wpdesk_cart_disclaimer_text() {
	echo '<div class="woocommerce-disclaimer_text">Online products feature discounted pricing; additional promotions or discounts do not apply.</div>';
}

add_action( 'woocommerce_before_add_to_cart_button', 'disclaimer_before_add_to_cart_btn' );
 
function disclaimer_before_add_to_cart_btn(){
    echo '<p class="woocommerce-disclaimer_text">Online products feature discounted pricing; additional promotions or discounts do not apply.</p>';
}
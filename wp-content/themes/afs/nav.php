<div class="navbar-header">
	<a class="navbar-brand" href="<?php bloginfo('url'); ?>">
		<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/afs-logo.png" alt="<?php bloginfo('name'); ?>" />
	</a>
</div>

<div class="navbar side-collapse in">
	<div class="controls">
		<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/logo-white.png" alt="<?php bloginfo('name'); ?>" />
		<button data-toggle="collapse-side" data-target=".side-collapse" type="button" class="navbar-toggle">
			<span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
		</button>
	</div>
	<nav role="navigation" class="navbar-collapse">
		<?php

			if (has_nav_menu('secondary_navigation')) :
				wp_nav_menu(array('theme_location' => 'secondary_navigation', 'menu_class' => 'nav nav-secondary'));
			endif;

			if (has_nav_menu('primary_navigation')) :
				wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav nav-main'));
			endif;

		?>
	</nav>
</div>

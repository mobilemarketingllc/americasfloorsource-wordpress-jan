<?php
	//Template Name: Special Promotions

get_header();

$promotion_id = get_field('promotion_id');
$promo_options = get_field('promo_options');

$header_type = get_field('header_type');

?>

<main id="<?php echo $promotion_id; ?>" role="main">

    <!-- Section: Header Home -->
	<?php
		 if ($header_type === 'split') :
			$header_bg = get_field('bg_split_header');
			$header_bg_mb = get_field('bg_split_header_mb');
	?>
			<section class="header-split">
				<div class="header-split__image">
					<picture>
						<source media="(min-width: 1200px)" srcset="<?php echo $header_bg['url']; ?>">
						<source media="(max-width: 1199px)" srcset="<?php echo $header_bg_mb['url']; ?>">
						<img src="<?php echo $header_bg['url'] ?>" alt="<?php echo $header_bg['alt'] ?>">
					</picture>
				</div>
				<div class="header-split__content">
					<div class="header-split__content-container">
						<?php the_field('split_header_content'); ?>
					</div>
				</div>
			</section>
	<?php
		else :
			$header_bg_d = get_field('header_bg_promotion_d');
			$header_bg_m = get_field('header_bg_promotion_m');
	?>
			<section class="afs-section header-home header-home-promotion">
				<picture>
					<source media="(min-width: 1200px)" srcset="<?php echo $header_bg_d['sizes']['hero-full']; ?>">
					<source media="(min-width: 768px)" srcset="<?php echo $header_bg_d['sizes']['promo-lg']; ?>">
					<source media="(max-width: 767px)" srcset="<?php echo $header_bg_m['url']; ?>">
					<img src="<?php echo $header_bg_d['url']; ?>" alt="<?php echo $header_bg_d['alt']; ?>">
				</picture>
			</section>
	<?php endif; ?>



    <?php if (have_rows('flexible_content')) : ?>


    <?php
	while (have_rows('flexible_content')) : the_row();

		if (get_row_layout() == 'promotions') :

			get_template_part('components/promotion/component', 'promo-promos');

		elseif (get_row_layout() == 'contest_form') :

			get_template_part('components/promotion/component', 'promo-form');

		elseif (get_row_layout() == '3_column_layout') :

			get_template_part('components/promotion/component', 'promo-3col');

		elseif (get_row_layout() == '5_column_layout') :

			get_template_part('components/promotion/component', 'promo-5col');

		elseif (get_row_layout() == 'financial_cta') :

			get_template_part('components/promotion/component', 'financingcta');

		elseif (get_row_layout() == 'simple_content') :

			get_template_part('components/promotion/component', 'promo-simple');

		elseif (get_row_layout() == 'simple_content_w_background') :

			get_template_part('components/promotion/component', 'promo-simple-bg');

		elseif (get_row_layout() == 'small_logos') :

			get_template_part('components/promotion/component', 'promo-logos');

		elseif (get_row_layout() == 'wholesale_2018') :

			get_template_part('components/promotion/component', 'promo-wholesale-2018');

		elseif (get_row_layout() == 'map_box') :

			get_template_part('components/promotion/component', 'promo-mapbox');

		elseif (get_row_layout() == '50-50') :

			get_template_part('components/promotion/component', 'promo-5050');

		endif;

	endwhile;
	?>



    <?php endif ?>

    <?php
	if ($promo_options['show_split_cta']) :
		include('components/component-splitcta.php');
	endif;
	?>



</main>

<?php

get_footer();

?>

<?php

// Creating the widget
class afs_tats extends WP_Widget {

	function __construct() {
			parent::__construct(
			// Base ID of your widget
			'afs_tats',

			// Widget name will appear in UI
			__('Recent Tips & Trends', 'afs_tats_domain'),

			// Widget description
			array( 'description' => __( 'Gets the 5 most recent Tips & Trends', 'afs_tats_domain' ), )
		);
	}

	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		// $title = apply_filters( 'widget_title', $instance['title'] );

		// before and after widget arguments are defined by themes
		echo $args['before_widget'];

		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $instance['title'] . $args['after_title'];
		}

		// Get and echo latest Tips and Trends
		$recent_tats = wp_get_recent_posts(	array(
												'post_type'=> 'tips_and_trends',
												'posts_per_page'=> '5'
											));

		if($recent_tats) {

			echo '<div class="widget-recent-tats">';
			echo 	'<h3 class="recent-tats-title">' . $instance['title'] . '</h3>';
			echo 	'<ul class="recent-tats-list">';

				foreach( $recent_tats as $tat ){
				    echo '<li><a href="' . get_permalink($tat["ID"]) . '" title="'.esc_attr($tat["post_title"]).'" >' .   $tat["post_title"].'</a> </li> ';
				}

			echo 	'</ul>';
			echo '</div>';
		}

		echo $args['after_widget'];
	}

	// Widget Backend
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'New title', 'afs_tats_domain' );
		}

			// Widget admin form
			?>
				<p>
					<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
					<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
				</p>
			<?php
		}

		// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}
} // Class afs_tats ends here

// Register and load the widget
function wpb_load_widget() {
	register_widget( 'afs_tats' );
}
add_action( 'widgets_init', 'wpb_load_widget' );
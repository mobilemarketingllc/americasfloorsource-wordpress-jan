<?php
/*
Add class(es) to next and previous Posts Links
*/
add_filter('next_posts_link_attributes', 'posts_link_attributes');
add_filter('previous_posts_link_attributes', 'posts_link_attributes');

function posts_link_attributes() {
		return 'class="btn btn-primary btn-sm"';
}

/*
Default WP Pagination
*/
function default_paginate() {
?>
	<div class="row">
		<div class="nav-previous col-xs-6 text-left">
			<?php previous_posts_link( '<i class="fa fa-chevron-circle-left"></i>&nbsp;&nbsp;'.__('Previous Page (newer posts)','bldwpjs') );
?>
		</div>
		<div class="nav-next col-xs-6 text-right">
			<?php next_posts_link( __('Next Page (older posts)','bldwpjs').'&nbsp;&nbsp;<i class="fa fa-chevron-circle-right"></i>' );
?>
		</div>
	</div>
<?php
}

<?php

/*
Social Links
*/

function social_links($class) {
	$fb_link = get_field('site_option_facebook_link', 'option');
	$tw_link = get_field('site_option_twitter_link', 'option');
	$gp_link = get_field('site_option_google_plus_link', 'option');
	$in_link = get_field('site_option_instagram_link', 'option');
	$pi_link = get_field('site_option_pinterest_link', 'option');
	$yt_link = get_field('site_option_youtube_link', 'option');
	$houzz_link = get_field('site_option_houzz_link', 'option');

	if ( $fb_link != '' || $tw_link != '' || $gp_link != '' || $in_link != '' || $pi_link != '' || $yt_link != '' ) {

	?>

	<ul class="<?php echo $class; ?> social-links-list">

		<?php if ( $fb_link != '' ) { ?>
		<li class="social-link-facebook">
			<a class="brand-facebook" href="<?php echo $fb_link; ?>" target="_blank" title="Visit Us On Facebook">
				<i class="fa fa-facebook"></i>
			</a>
		</li>
		<?php } ?>

		<?php if ( $tw_link != '' ) { ?>
		<li class="social-link-twitter">
			<a class="brand-twitter" href="<?php echo $tw_link; ?>" target="_blank" title="Visit Us On Twitter">
				<i class="fa fa-twitter"></i>
			</a>
		</li>
		<?php } ?>

		<?php if ( $gp_link != '' ) { ?>
		<li class="social-link-google-plus">
			<a class="brand-google" href="<?php echo $gp_link; ?>" target="_blank" title="Visit Us On Google Plus">
				<i class="fa fa-google-plus"></i>
			</a>
		</li>
		<?php } ?>

		<?php if ( $in_link != '' ) { ?>
		<li class="social-link-instagram">
			<a class="brand-instagram" href="<?php echo $in_link; ?>" target="_blank" title="Visit Us On Instagram">
				<i class="fa fa-instagram"></i>
			</a>
		</li>
		<?php } ?>

		<?php if ( $pi_link != '' ) { ?>
		<li class="social-link-pinterest">
			<a class="brand-pinterest" href="<?php echo $pi_link; ?>" target="_blank" title="Visit Us On Pinterest">
				<i class="fa fa-pinterest"></i>
			</a>
		</li>
		<?php } ?>

		<?php if ( $yt_link != '' ) { ?>
		<li class="social-link-youtube">
			<a class="brand-youtube" href="<?php echo $yt_link; ?>" target="_blank" title="Visit Us On Youtube">
				<i class="fa fa-youtube"></i>
			</a>
		</li>
		<?php } ?>

		<?php if ( $houzz_link != '' ) { ?>
		<li class="social-link-houzz">
			<a class="brand-houzz" href="<?php echo $houzz_link; ?>" target="_blank" title="Visit Us On Houzz">
				<i class="fa fa-houzz" aria-hidden="true"></i>
			</a>
		</li>
		<?php } ?>

	</ul>

	<?php
	}
}

?>

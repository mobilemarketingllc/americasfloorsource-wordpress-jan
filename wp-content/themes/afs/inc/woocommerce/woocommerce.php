<?php
	/*
	Add Woocommerce Theme Support
	*/
	add_action( 'after_setup_theme', 'woocommerce_support' );
	function woocommerce_support() {
		add_theme_support( 'woocommerce' );
	}


	include('wc-functions.php');
	include('myaccount.php');
	include('forms.php');
	include('hooks.php');
	include('overrides.php');
	include('waiting-for-pickup.php');

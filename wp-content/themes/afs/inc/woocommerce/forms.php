<?php

    /**
     * Add placeholders to form fields and make the placeholder and label consistant
     */
    add_filter( 'woocommerce_default_address_fields' , 'custom_override_default_address_fields', 20, 1 );
    function custom_override_default_address_fields( $address_fields ) {
        $address_fields['first_name']['placeholder'] = 'First Name';
        $address_fields['first_name']['label'] = 'First Name';

        $address_fields['last_name']['placeholder'] = 'Last Name';
        $address_fields['last_name']['label'] = 'Last Name';

        $address_fields['company']['placeholder'] = 'Company Name';
        $address_fields['company']['label'] = 'Company Name';

        $address_fields['country']['placeholder'] = 'Country';
        $address_fields['country']['label'] = 'Country';

        $address_fields['address_1']['placeholder'] = 'Address';
        $address_fields['address_1']['label'] = 'Address';

        $address_fields['address_2']['placeholder'] = 'Address - Line 2';
        $address_fields['address_2']['label'] = 'Address - Line 2';

        $address_fields['state']['placeholder'] = 'State';
        $address_fields['state']['label'] = 'State';

        $address_fields['city']['placeholder'] = 'City';
        $address_fields['city']['label'] = 'City';

        $address_fields['postcode']['placeholder'] = 'Zip Code';
        $address_fields['postcode']['label'] = 'Zip Code';


        return $address_fields;
    }

    /**
     * Add placeholder and additional classes to Phone and Email on Billing Form
     */
    add_filter( 'woocommerce_billing_fields' , 'override_billing_checkout_fields', 20, 1 );
    function override_billing_checkout_fields( $fields ) {
        $fields['billing_phone']['placeholder'] = 'Phone';
        $fields['billing_phone']['label'] = 'Phone';
        $fields['billing_phone']['class'] = array('form-row-first');


        $fields['billing_email']['placeholder'] = 'Email';
        $fields['billing_email']['label'] = 'Email';
        $fields['billing_email']['class'] = array('form-row-last');
        return $fields;
    }



    /**
     * @snippet       Add First & Last Name to My Account Register Form - WooCommerce
     * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
     * @sourcecode    https://businessbloomer.com/?p=21974
     * @author        Rodolfo Melogli
     * @credits       Claudio SM Web
     * @compatible    WC 3.4
     */

    /**
     * Add fields to form
     */
    add_action( 'woocommerce_register_form_start', 'register_form_add_name' );
    function register_form_add_name() {
?>

    <p class="form-row">
        <label for="reg_billing_first_name"><?php _e( 'First name', 'woocommerce' ); ?> <span class="required">*</span></label>
        <input
            type="text"
            class="input-text"
            name="billing_first_name"
            id="reg_billing_first_name"
            placeholder="First Name"
            value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" />
    </p>

    <p class="form-row">
        <label for="reg_billing_last_name"><?php _e( 'Last name', 'woocommerce' ); ?> <span class="required">*</span></label>
        <input
            type="text"
            class="input-text"
            name="billing_last_name"
            id="reg_billing_last_name"
            placeholder="Last Name"
            value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" />
    </p>

    <div class="clear"></div>

<?php
    }


    /**
     * Validate name
     */
    add_filter( 'woocommerce_registration_errors', 'register_form_validate_name', 10, 3 );
    function register_form_validate_name( $errors, $username, $email ) {

        if ( isset( $_POST['billing_first_name'] ) && empty( $_POST['billing_first_name'] ) ) {
            $errors->add( 'billing_first_name_error', __( '<strong>Error</strong>: First name is required!', 'woocommerce' ) );
        }

        if ( isset( $_POST['billing_last_name'] ) && empty( $_POST['billing_last_name'] ) ) {
            $errors->add( 'billing_last_name_error', __( '<strong>Error</strong>: Last name is required!.', 'woocommerce' ) );
        }

        return $errors;
    }

    /**
     * Save name on registration
     */
    add_action( 'woocommerce_created_customer', 'register_form_save_name' );
    function register_form_save_name( $customer_id ) {

        if ( isset( $_POST['billing_first_name'] ) ) {
            update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
            update_user_meta( $customer_id, 'first_name', sanitize_text_field($_POST['billing_first_name']) );
        }

        if ( isset( $_POST['billing_last_name'] ) ) {
            update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
            update_user_meta( $customer_id, 'last_name', sanitize_text_field($_POST['billing_last_name']) );
        }

    }
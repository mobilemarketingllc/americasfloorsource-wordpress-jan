<?php
// Register Nav
register_nav_menus(array(
	'primary_navigation' => __('Primary Navigation', 'bldwpjs'),
	'secondary_navigation' => __('Secondary Navigation', 'bldwpjs'),
	'footer-sitemap-1' => __('Footer Sitemap #1', 'bldwpjs'),
	'footer-sitemap-2' => __('Footer Sitemap #2', 'bldwpjs'),
	'footer-locations' => __('Footer Locations', 'bldwpjs'),
	'about-template' => __('About Side Menu', 'bldwpjs'),
	'b2b-template' => __('B2B Side Menu', 'bldwpjs'),
	'page-404' => __('404 Menu', 'bldwpjs'),
));

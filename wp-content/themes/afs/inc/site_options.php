<?php
/*
Add theme options in WP Admin. Use for global data.
*/
if( function_exists('acf_add_options_page') ) {

 	// add parent
	$parent = acf_add_options_page(array(
		'page_title' 	=> 'Site Options',
		'menu_title' 	=> 'Site Options',
		'redirect' 		=> false
	));

}

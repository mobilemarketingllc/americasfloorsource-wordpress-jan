<?php
/*
Gravity Forms - Enable field visibility options
*/
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

/*
Gravity Forms - Style Button
*/
function form_submit_button($button,$form){
		return '<input type="submit" class="btn--rounded" id="gform_submit_button_' . $form['id'] . '" value="' . $form['button']['text'] . '">';
}
add_filter('gform_submit_button','form_submit_button',10,2);

// Replace ajax spinner with blank pixel; Use CSS to style object
add_filter("gform_ajax_spinner_url", "spinner_url", 10, 2);
function spinner_url($image_src, $form){
	return  get_bloginfo('template_directory') . '/assets/img/blank.gif' ; // relative to you theme images folder
}

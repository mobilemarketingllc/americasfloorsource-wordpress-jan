<?php
	//Component: Split CTA
	$splitcta_bg['scta-L'] = get_field('split_cta_bg_left', 'options');
	$splitcta_bg['scta-R'] = get_field('split_cta_bg_right', 'options');

	$ecomEnabled = get_field('enable_ecommerce', 'options');
?>

<section class="afs-section component-scta <?php echo ($ecomEnabled) ? 'component-scta--with-ecom' : '';?>">
	<?php
		echo responsive_backgrounds_half($splitcta_bg);
	?>

	<div class="container-fluid">
		<div class="row">
			<div class="scta-L col-md-6">
				<div class="scta-container">
					<div class="scta-content">
						<?php the_field('split_cta_content_left', 'option')?>
					</div>
				</div>
			</div>

			<div class="scta-R col-md-6">
				<div class="scta-container">
					<div class="scta-content">
						<?php the_field('split_cta_content_right', 'option')?>
					</div>
				</div>
			</div>


			<?php
				if($ecomEnabled):
					$splitcta_e['scta-E'] = get_field('split_cta_bg_ecom', 'options');
					echo responsive_backgrounds_full($splitcta_e);
			?>
				<div class="scta-E col-sm-12">
					<div class="scta-container">
						<div class="scta-content">
							<?php the_field('split_cta_content_ecom', 'option')?>
						</div>
					</div>
				</div>
			<?php endif; ?>

		</div>
	</div>
</section>
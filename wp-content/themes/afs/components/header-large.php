<?php
$header_img = get_field("header_bg");
$header_bg['header-large'] = $header_img;
$header_title = get_field('header_title') ?: get_the_title();
$header_title_style = (wp_get_post_parent_id(get_the_ID()) != 0) ? ' header-title-small' : '';

$style = get_field('header_style');
?>

<section class="afs-section header-large header-large-<?php echo $style; ?>">
	<?php if ($style == 'complex' || $style == 'simple') : ?>
		<?php echo responsive_backgrounds_full($header_bg); ?>
	<?php endif; ?>

	<?php if ($style == 'complex') : ?>
		<div class="container">
			<div class="row">
				<div class="header-title<?php echo $header_title_style; ?>">
					<?php echo $header_title; ?>
				</div>
			</div>
		</div>

		<div class="component-graytitle">
			<div class="graytitle-text">
				<?php echo get_field('header_footer') ?>
			</div>
		</div>
	<?php elseif ($style == 'scale') : ?>
		<img src="<?php echo $header_img['url']; ?>" alt="<?php echo $header_img['alt']; ?>">
	<?php endif ?>

</section>

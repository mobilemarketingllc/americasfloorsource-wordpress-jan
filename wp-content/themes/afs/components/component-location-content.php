<?php
	$watermark = get_field('placeholder_watermark', 'options');

	//Info
	$location_address = format_address( get_the_ID() );
	$location_gmap_link = get_field("location_gmap_link");
	$location_phone = get_field("location_phone");
	$location_additional_phone = get_field("location_additional_phone");


	//Reviews
	$location_reviews = get_field("location_review_urls");

	//Hours
	$location_hours = get_field("location_hours");

	$location_content = get_field('location_content');

?>


<!-- Header: Location -->
<section class="afs-section afs-location-v2">

	<div class="container">
		<div class="row">
			<div class="afs-location-v2__content afs-CAS-1 col-md-9">
				<h1 class="afs-title-lg"><?php the_title(); ?></h1>

				<!-- Put Content Here -->
				<?php
					foreach ($location_content as $key => $section):


						switch ($section['acf_fc_layout']) {
							case 'location_simple':

								?>

									<div class="afs-location-v2__simple">
										<?php echo $section['content'] ?>
									</div>


								<?php

								break;

							case 'location_product':

									//Component: Product Bar
									$args = array(
										'post_type' => 'page',
										'post__in' => $section['products'],
										'order' => 'ASC',
										'orderby' => 'menu_order'
									);

									$products = new WP_Query($args);

									if($products->have_posts()) :

								?>

										<section class="afs-location-v2__product">
											<h2 class="afs-title-alt">Check out these amazing flooring products available now at our showroom:</h2>
											<div class="component-productbar">
												<div class="productbar-container">
													<?php
														while($products->have_posts()) :

															$products->the_post();
													?>
															<div class="product" style="background-image: url('<?php echo get_field('product_bar_image')['sizes']['thumbnail'] ?>')">
																<a class="product-title" href="<?php the_permalink(); ?>">
																	<?php the_title(); ?>
																</a>
															</div>

													<?php
														endwhile;
													?>
												</div>
											</div>
										</section>

								<?php
									endif;

									wp_reset_postdata();

								break;

							case 'location_2col':

								?>

									<div class="afs-location-v2__2col">
										<div class="column column-1">
											<?php echo $section['column_1']['content'] ?>

											<?php if ($section['column_1']['link']): ?>
												<a class="btn--square" href="<?php echo $section['column_1']['link']['url'] ?>"><?php echo $section['column_1']['link']['title'] ?></a>
											<?php endif ?>

										</div>
										<div class="column column-2">
											<?php echo $section['column_2']['content'] ?>

											<?php if ($section['column_2']['link']): ?>
												<a class="btn--square" href="<?php echo $section['column_2']['link']['url'] ?>"><?php echo $section['column_2']['link']['title'] ?></a>
											<?php endif ?>
										</div>
									</div>


								<?php

								break;

						}

					endforeach;
				?>
			</div>

			<div class="afs-location-v2__sidebar col-md-3">
				<div class="location-info">
					<h3>Location Information</h3>
					<div class="location-contact">
						<?php if ($location_address && $location_gmap_link): ?>
							<address class="location-address">
								<a href="<?php echo $location_gmap_link; ?>" target="_blank">
									<?php echo $location_address; ?>
								</a>
							</address>
						<?php endif ?>

						<?php if ($location_phone): ?>
							<a class="location-phone" href="tel:+1<?php echo $location_phone; ?>"><?php echo format_phone( $location_phone ); ?></a>
						<?php endif ?>

						<?php if ($location_additional_phone): ?>

							<ul class="location-addphone">
								<?php foreach ($location_additional_phone as $key => $phone): ?>
									<li>
										<h6 class="location-addphone-label"><?php echo $phone['phone_label']; ?></h6>
										<a class="location-phone" href="tel:+1<?php echo $phone['phone_number']; ?>"><?php echo format_phone( $phone['phone_number'] ); ?></a>
									</li>
								<?php endforeach ?>
							</ul>

						<?php endif ?>

					</div>

					<?php if (!empty($location_reviews)): ?>
						<div class="location-review">
							<?php foreach ($location_reviews as $key => $network):

								if(!$network['item_show']) {
									continue;
								}

							?>
								<a class="review-<?php echo $network['item_network']['value']; ?>" href="<?php echo $network['item_url']; ?>" target="_blank"><?php echo $network['item_network']['label']; ?> Reviews</a>
							<?php endforeach ?>
						</div>
					<?php endif ?>
				</div>

				<?php if ($location_hours): ?>
					<div class="location-hours">

						<?php foreach ($location_hours as $key => $set):
							if (!empty($set['hours'])): ?>
								<div class="hours<?php echo (!empty($set['info']['footnote'])) ? ' hours-footer' : ''; ?>">
									<h3><?php echo $set['info']['label']; ?></h3>

									<?php foreach ($set['hours'] as $key => $day): ?>
										<dl>
											<dt><?php echo $day['day'] ?></dt>
											<dd><?php echo $day['time'] ?></dd>
										</dl>
									<?php endforeach;

									if(!empty($set['info']['footnote'])) : ?>

										<span class="footnote"><?php echo $set['info']['footnote'] ?></span>

								<?php
									endif;
								?>

								</div>
						<?php
							endif;
						endforeach; ?>
					</div>
				<?php endif ?>

				<?php if (get_field("content")): ?>

					<div class="afs-designers">
						<?php the_field("content"); ?>
					</div>

				<?php endif ?>

			</div>
		</div>
	</div>
</section>
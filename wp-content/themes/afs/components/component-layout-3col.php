<div class="afs-about-c3 afs-CAS-1 col-lg-9 col-md-9">
	<?php echo $section['3col_lead']; ?>

	<?php if ($section['3col_col_1']): ?>
		<div class="col-lg-4">
			<?php echo $section['3col_col_1']; ?>
		</div>
	<?php endif ?>

	<?php if ($section['3col_col_2']): ?>
		<div class="col-lg-4">
			<?php echo $section['3col_col_2']; ?>
		</div>
	<?php endif ?>

	<?php if ($section['3col_col_3']): ?>
		<div class="col-lg-4">
			<?php echo $section['3col_col_3']; ?>
		</div>
	<?php endif ?>

</div>


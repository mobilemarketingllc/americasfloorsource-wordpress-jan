<?php
	get_header();

	$header_bg['afs-map-embed'] = get_field("location_photo_map_lg");
	$smcta_bg['afs-schedulecta'] = get_field('sm_cta_bg', 'option');

	$location_map_embed = get_field("location_map_embed");

?>

<main id="main" role="main">

	<?php

		if(get_field('location_content')) {

			if ($location_map_embed):

	?>

				<section class="afs-map-embed top">
					<?php echo responsive_backgrounds_full($header_bg); ?>
					<?php echo $location_map_embed; ?>
				</section>

	<?php
			endif;


			include('components/component-location-content.php');

		} else {

			include('components/component-location-nocontent.php');

		}


	?>


	<!-- Section: Schedule Appointment -->
	<?php include('components/component-schedulecta.php'); ?>


	<?php if ($location_map_embed && !get_field('location_content')): ?>
		<!-- Section: Map for Locations without content  -->
		<section class="afs-map-embed bottom">
			<?php echo responsive_backgrounds_full($header_bg); ?>
			<?php echo $location_map_embed; ?>
		</section>

	<?php endif; ?>

	<!-- Section: Testimonial -->
	<?php include('components/component-testimonial.php'); ?>




</main>

<?php

	get_footer();

?>

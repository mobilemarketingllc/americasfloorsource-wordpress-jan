<?php
/**
 * My Account page
 *
 * Modifications
 * - Add div.woocommerce-MyAccount-container wrapper so that we can control the navigation and content with flex
 * - Add afs_woocommerce_header for breadcrumbs and links
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

afs_woocommerce_header();

?>

<div class="woocommerce-MyAccount-container">
	<?php

		/**
		 * My Account navigation.
		 * @since 2.6.0
		 */
		do_action( 'woocommerce_account_navigation' );
	?>

	<div class="woocommerce-MyAccount-content">
		<?php
			/**
			 * My Account content.
			 * @since 2.6.0
			 */
			do_action( 'woocommerce_account_content' );
		?>
	</div>
</div>

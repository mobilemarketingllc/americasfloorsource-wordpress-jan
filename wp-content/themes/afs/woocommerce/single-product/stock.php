<?php
/**
 * Single Product stock.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/stock.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * Modifiations:
 * - Add "Only"
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;

if($product->managing_stock()) {
	$stock = $product->get_stock_quantity();

	if(isCarton($product)) {
		$sqftpc = get_field('sqft_per_carton', $product->get_ID());
		$stock_quantity = number_format(round(($sqftpc * $stock), 2), 2) . ' sqft';
	} else {
		$stock_quantity = number_format(round($stock, 2), 2) . ' sqft';
	}

	$availability = sprintf( __( 'Only %s left in stock', 'woocommerce' ), $stock_quantity );
}

?>

<p class="stock <?php echo esc_attr( $class ); ?>"><?php echo wp_kses_post( $availability ); ?></p>

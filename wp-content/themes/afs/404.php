<?php get_header(); ?>


<main id="main" role="main">
	<!-- Section: No Query Results -->
	<section class="afs-section component-no-grid">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="afs-title-lg">Whoops!</h1>
					<p class="afs-body-main">
						Looks like there isn't anything here.</br>
						Check out these pages and maybe you'll find what you are looking for.
					</p>

					<div class="afs-404-menu">
						<?php

							if (has_nav_menu('page-404')):
								wp_nav_menu(array('theme_location' => 'page-404', 'walker' => new Walker_Nav_Menu));
							endif;

						?>
					</div>

				</div>
			</div>
		</div>
	</section>
</main>

<?php get_footer(); ?>

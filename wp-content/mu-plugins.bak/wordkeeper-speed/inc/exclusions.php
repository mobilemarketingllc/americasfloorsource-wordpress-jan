<?php

if(in_array('leadpages/leadpages.php', $wordkeeper_active_plugins)) {
	$wordkeeper_overrides['leadpages/leadpages.php']['defer-javascript'] = false;
	$wordkeeper_overrides['leadpages/leadpages.php']['lazyload-images'] = false;
	$wordkeeper_overrides['leadpages/leadpages.php']['lazyload-videos'] = false;
	$wordkeeper_overrides['leadpages/leadpages.php']['use-deferscript'] = false;

	/**
	 * wordkeeper_filter_leadpages function.
	 *
	 * @access public
	 * @param mixed $function
	 * @param mixed $buffer
	 * @return void
	 */
	function wordkeeper_filter_leadpages($function, $buffer) {
		global $wordkeeper, $wordkeeper_map, $wordkeeper_overrides, $wordkeeper_filtered;

		// Map function to setting for review
		$setting = $wordkeeper_map[$function];

		// If already filtered, then return the filtered setting
		if(isset($wordkeeper_filtered[$setting]) && $wordkeeper_filtered[$setting] === true) {
			return $wordkeeper_overrides['leadpages/leadpages.php'][$setting];
		}

		// Disable feature if on a LeadPage
		if(strpos($buffer, '<meta name="leadpages-served-by" content="wordpress">') !== false && isset($wordkeeper_overrides['leadpages/leadpages.php'][$setting])) {
			$wordkeeper_filtered[$setting] = true;

			return $wordkeeper_overrides['leadpages/leadpages.php'][$setting];
		}

		return $wordkeeper[$setting];
	}
	add_filter('wordkeeper_filter_overrides', 'wordkeeper_filter_leadpages', 10, 2);	
}

<?php
/**
 *
 * @link              https://wordkeeper.com
 * @since             1.0.0
 * @package           WordKeeper Speed Optimization
 *
 * @wordpress-plugin
 * Plugin Name:       WordKeeper Speed Optimization
 * Plugin URI:        https://wordkeeper.com/plugins/optimization
 * Description:       WordKeeper Speed Optimization
 * Version:           1.0.0
 * Author:            WordKeeper
 * Author URI:        https://wordkeeper.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wordkeeper-speed
 * Domain Path:       /languages
 */


// If this file is called directly, abort.
if(!defined('WPINC')) {
	die;
}

/**
 * wordkeeper_buffer_start function.
 *
 * Start output buffering
 *
 * @access public
 * @return void
 */
if($wordkeeper['use-buffering']) {
	function wordkeeper_buffer_start() {
		if (!is_admin()) {
			ob_start('wordkeeper_buffer_callback');
		}
	}
}


/**
 * wordkeeper_buffer_stop function.
 *
 * Stop output buffering
 *
 * @access public
 * @return void
 */
if($wordkeeper['use-buffering']) {
	function wordkeeper_buffer_stop() {
		if (!is_admin()) {
			ob_end_flush();
		}
	}
}


/**
 * wordkeeper_buffer_callback function.
 *
 * Output buffering callback
 *
 * @access public
 * @param mixed $buffer
 * @return void
 */
function wordkeeper_buffer_callback($buffer) {
	global $wordkeeper;

	// Check for a Content-Type header. Currently only apply rewriting to "text/html" or undefined
	$headers = headers_list();
	$content_type = null;

	foreach($headers as $header) {
		if(strpos(strtolower($header), 'content-type:') === 0) {
			$pieces = explode(':', strtolower($header));
			$content_type = trim($pieces[1]);
			break;
		}
	}

	if(is_null($content_type) || substr($content_type, 0, 9) === 'text/html') {

		apply_filters_ref_array('wordkeeper_before_buffer_edit', array(&$buffer));

		preg_match_all('/(?s)<\s?noscript.*?(?:\/\s?>|<\s?\/\s?noscript\s?>)/', $buffer, $noscripts);
		if(!empty($noscripts)) {
			$noscripts = $noscripts[0];
		}

		// Capture iframes for video optimization
		if($wordkeeper['capture-iframes'] && !$wordkeeper['amp']) {
			preg_match_all('/(?s)<\s?iframe.*?(?:\/\s?>|<\s?\/\s?iframe\s?>)/', $buffer, $iframes);

			$frames = array();
			$frames['iframes'] = array();
			$frames['videos'] = array();

			if(!empty($iframes)) {
				$iframes = $iframes[0];
				$iframes = wordkeeper_filter_noscript('iframe', $noscripts, $iframes);

				foreach ($iframes as $iframe) {
					if($wordkeeper['lazyload-videos'] || $wordkeeper['lazyload-iframes']) {
						if(
							strpos($iframe, 'youtube.com/embed/') ||
							strpos($iframe, 'youtu.be/')
						) {
							$frames['videos'][] = array('video' => $iframe, 'type' => 'youtube');
						}
						elseif(strpos($iframe, 'https://player.vimeo.com/video/') !== false) {
							$frames['videos'][] = array('video' => $iframe, 'type' => 'vimeo');
						}
						elseif(strpos($iframe, '//fast.wistia.net/embed/iframe/') !== false) {
							$frames['videos'][] = array('video' => $iframe, 'type' => 'wistia');
						}
						elseif(strpos($iframe, 'gform_ajax_frame') !== false) {
							// Skip Gravity Forms iframe
							continue;
						}
						else {
							$frames['iframes'][] = $iframe;
						}
					}
				}
			}

			apply_filters_ref_array('wordkeeper_buffer_iframes_edit', array(&$buffer, &$frames));
		}

		// Capture images for lazy loading
		if($wordkeeper['capture-images'] && !$wordkeeper['amp']) {
			preg_match_all('/(?s)<\s?img[^>]*>/', $buffer, $images);

			if(!empty($images)) {
				$images = $images[0];
				$images = wordkeeper_filter_noscript('img', $noscripts, $images);
				apply_filters_ref_array('wordkeeper_buffer_images_edit', array(&$buffer, &$images));
			}
		}

		// Capture scripts for JS/video optimization
		if($wordkeeper['capture-scripts']) {
			preg_match_all('/(?s)<\s?script.*?(?:\/\s?>|<\s?\/\s?script\s?>)/', $buffer, $scripts);
			if(!empty($scripts)) {
				$scripts = $scripts[0];
				apply_filters_ref_array('wordkeeper_buffer_scripts_edit', array(&$buffer, &$scripts));
			}
		}

		// Capture scripts for JS/video optimization
		if($wordkeeper['capture-links']) {
			preg_match_all('/(?s)<\s?link.*?(?:\/\s?>|<\s?\/\s?link\s?>)/', $buffer, $links);
			if(!empty($links)) {
				$links = $links[0];
				$links = wordkeeper_filter_noscript('link', $noscripts, $links);
				apply_filters_ref_array('wordkeeper_buffer_links_edit', array(&$buffer));
			}
		}
	}

	apply_filters_ref_array('wordkeeper_after_buffer_edit', array(&$buffer));

	return $buffer;
}


/**
 * wordkeeper_filter_noscript function.
 *
 * @access public
 * @param mixed $noscripts
 * @param mixed $collection
 * @return array $collection
 */
function wordkeeper_filter_noscript($type, &$noscripts, &$collection) {
	$filtered = $collection;

	foreach($noscripts as $noscript) {
		if(strpos($noscript, '<' . $type) !== false) {
			for($i=0; $i < count($collection); $i++) {
				if(strpos($noscript, $collection[$i]) !== false) {
					unset($filtered[$i]);
				}
			}
		}
	}

	return $filtered;
}


/**
 * wordkeeper_lazyload_video function.
 *
 * @access public
 * @param mixed $video
 * @param mixed $buffer
 * @return string $buffer
 */
function wordkeeper_lazyload_video($video, &$buffer) {
	global $wordkeeper;

	preg_match('#src=[\'"]([^\'"]*)[\'"]#', $video['video'], $src);

	$src = explode('?', $src[1]);
	$src = trim($src[0], '/');
	$src = str_replace('https://', '', $src);
	$src = str_replace('http://', '', $src);
	$src = str_replace('//', '', $src);

	switch($video['type']) {
		case 'youtube':
			$src = str_replace('www.youtube.com/embed/', '', $src);
			$src = str_replace('youtube.com/embed/', '', $src);
			$src = str_replace('youtu.be/', '', $src);
			$src = explode('/', $src);
			$embed = $src[0];
			$datasrc = 'https://www.youtube.com/watch?v=' . $embed . '&autoplay=1&rel=0&controls=0&showinfo=0';
			break;
		case 'vimeo':
			$src = str_replace('player.vimeo.com/video/', '', $src);
			$src = explode('/', $src);
			$embed = $src[0];
			$datasrc = 'https://vimeo.com/' . $embed . '?color=949494&title=0&byline=0&portrait=0&autoplay=1';
			break;
		case 'wistia':
			$src = str_replace('fast.wistia.net/embed/iframe/', '', $src);
			$src = trim($src, '/');
			$src = explode('/', $src);
			$embed = $src[0];
			$datasrc = 'javascript:;';
			break;
		default:
			break;
	}

	preg_match('#width=[\'"]*([0-9%]+)[\'"]*#', $video['video'], $width);

	$width = (!empty($width)) ? $width[1] : null;
	$styles = '';

	if($wordkeeper['lazyload-videos-mode'] == 'default') {
		$template = trim(file_get_contents(dirname(__FILE__) . '/templates/lazyload.video.html'));
	}
	else {
		$template = trim(file_get_contents(dirname(__FILE__) . '/templates/lightbox.video.html'));
	}
	$replace = str_replace('{type}', $video['type'], $template);
	$replace = str_replace('{embed}', $embed, $replace);

	if(!empty($width) && strpos($width, '%') === false) {
		$widthtype = ($width >= 525) ? 'max-width' : 'min-width';
		$styles .= ' ' . $widthtype . ': ' . $width . 'px;';
	}
	elseif(!empty($width) && strpos($width, '%') !== false) {
		$styles .= ' width: ' . $width . '%;';
	}

	$replace = str_replace('{styles}', 'style="' . $styles . '"', $replace);
	$replace = str_replace('{video}', $datasrc, $replace);

	if($video['type'] == 'wistia') {
		$replace = str_replace('data-fancybox', 'data-fancybox="iframe" data-type="iframe" data-src="//fast.wistia.net/embed/iframe/' . $embed . '?autoplay=1"', $replace);
	}

	$buffer = str_replace($video['video'], $replace, $buffer);
}


/**
 * wordkeeper_lazyload_image function.
 *
 * @access public
 * @param mixed $image
 * @param mixed $buffer
 * @return void
 */
function wordkeeper_lazyload_image($image, &$buffer) {
	$replaceimage = $image;

	// Handle src
	preg_match('#[\s\t\r\n]src=[\'"]?([^\'"]*)[\'"]?#', $image, $src);

	if(strpos($image, 'data-src') !== false) {
		preg_match('#data-src=[\'"]?([^\'"]*)[\'"]?#', $image, $datasrc);

		if(!empty($datasrc) && !empty($datasrc[1])) {
			if(!empty($src) && !empty($src[1]) && strpos($src[1], '//') !== false) {
				//$url = $src[1];
				$replaceimage = str_replace($datasrc[0], '', $replaceimage);
			}
			elseif(!empty($src)) {
				//$url = $datasrc[1];
				$replaceimage = str_replace($src[0], '', $replaceimage);
				$replaceimage = str_replace('data-src', 'src', $replaceimage);
			}
			else {
				$replaceimage = str_replace('data-src', 'src', $replaceimage);
			}
		}
		$replaceimage = str_replace('src=', 'data-src=', $replaceimage);
	}
	else {
		$replaceimage = str_replace('src=', 'data-src=', $replaceimage);
	}

	// Handle srcset
	preg_match('#[\s\t\r\n]srcset=[\'"]?([^\'"]*)[\'"]?#', $image, $src);

	if(strpos($image, 'data-srcset') !== false) {
		preg_match('#data-srcset=[\'"]?([^\'"]*)[\'"]?#', $image, $datasrc);

		if(!empty($datasrc) && !empty($datasrc[1])) {
			if(!empty($src) && !empty($src[1]) && strpos($src[1], '//') !== false) {
				//$url = $src[1];
				$replaceimage = str_replace($datasrc[0], '', $replaceimage);
			}
			elseif(!empty($src)) {
				//$url = $datasrc[1];
				$replaceimage = str_replace($src[0], '', $replaceimage);
				$replaceimage = str_replace('data-srcset', 'srcset', $replaceimage);
			}
			else {
				$replaceimage = str_replace('data-srcset', 'srcset', $replaceimage);
			}
		}
		$replaceimage = str_replace('srcset=', 'data-srcset=', $replaceimage);
	}
	else {
		$replaceimage = str_replace('srcset=', 'data-srcset=', $replaceimage);
	}

	if(strpos($image, 'class=') === false) {
		$replaceimage = str_replace('<img', '<img class="lazyload"', $replaceimage);
	}
	else {
		preg_match('#class=[\'"]?([^\'"]*)[\'"]?#', $image, $class);
		if(!empty($class) && !empty($class[1])) {
			$classes = explode(' ', $class[1]);
			$classes[] = 'lazyload';
			$replaceimage = str_replace($class[0], 'class="' . implode(' ', $classes) . '"', $replaceimage);
		}
	}

	$buffer = str_replace($image, $replaceimage, $buffer);
}


/**
 * wordkeeper_lazyload_iframe function.
 *
 * @access public
 * @param mixed $iframe
 * @param mixed $buffer
 * @return void
 */
function wordkeeper_lazyload_iframe($iframe, &$buffer) {
	if(strpos($iframe, 'calendar.google.com/calendar/embed') !== false) {
		return;
	}

	preg_match('#[\s\t\r\n]src=[\'"]?([^\'"]*)[\'"]?#', $iframe, $src);

	if(strpos($iframe, 'data-src') !== false) {
		preg_match('#data-src=[\'"]?([^\'"]*)[\'"]?#', $iframe, $datasrc);

		// If both src and data-src are empty, don't process the iframe
		if((empty($datasrc) || empty($datasrc[1])) && (empty($src) || empty($src[1]) || $src[1] = 'about:blank')) {
			return;
		}

		if(!empty($datasrc) && !empty($datasrc[1])) {
			if(!empty($src) && !empty($src[1]) && strpos($src[1], '//') !== false) {
				//$url = $src[1];
				$replaceframe = str_replace($datasrc[0], '', $iframe);
			}
			elseif(!empty($src)) {
				//$url = $datasrc[1];
				$replaceframe = str_replace($src[0], '', $iframe);
				$replaceframe = str_replace('data-src', 'src', $replaceframe);
			}
			else {
				$replaceframe = str_replace('data-src', 'src', $iframe);
			}
		}
		$replaceframe = str_replace('src=', 'data-src=', $replaceframe);
	}
	else {
		$replaceframe = str_replace('src=', 'data-src=', $iframe);
	}

	if(strpos($iframe, 'class=') === false) {
		$replaceframe = str_replace('<iframe', '<iframe class="lazyload"', $replaceframe);
	}
	else {
		preg_match('#class=[\'"]?([^\'"]*)[\'"]?#', $iframe, $class);
		if(!empty($class) && !empty($class[1])) {
			$classes = explode(' ', $class[1]);
			$classes[] = 'lazyload';
			$replaceframe = str_replace($class[0], 'class="' . implode(' ', $classes) . '"', $replaceframe);
		}
	}

	$buffer = str_replace($iframe, $replaceframe, $buffer);
}


/**
 * wordkeeper_lazyload_script_videos function.
 *
 * @access public
 * @param mixed $buffer
 * @return string $buffer
 */
function wordkeeper_lazyload_script_videos(&$buffer) {
	global $wordkeeper;

	if(strpos($buffer, 'fast.wistia.net') !== false) {
		$buffer = preg_replace('#<script[^>]*src="//fast\.wistia\.net/assets/external/E-v1\.js" async></script>#', '', $buffer);
		$buffer = str_replace('<script src="//fast.wistia.com/assets/external/E-v1.js" async></script>', '', $buffer);
		$buffer = preg_replace('#<div class="wistia_embed wistia_async_[^"]*" style="[^"]*">&nbsp;</div>#', '', $buffer);

		preg_match_all('#<script [^>]*src="//fast\.wistia\.com/embed/medias/([^>])*\.jsonp"[^>]*>*</script>#', $buffer, $wistias);
		foreach($wistias[0] as $wistia) {
			preg_match('#src=[\'"]([^\'"]*)[\'"]#', $wistia, $src);
			$src = explode('?', $src[1]);
			$src = str_replace('//fast.wistia.com/embed/medias/', '', $src[0]);
			$src = str_replace('.jsonp', '', $src);
			$embed = $src;

			if($wordkeeper['lazyload-videos-mode'] == 'default') {
				$template = trim(file_get_contents(dirname(__FILE__) . '/templates/lazyload.video.html'));
			}
			else {
				$template = trim(file_get_contents(dirname(__FILE__) . '/templates/lightbox.video.html'));
			}
			$replace = str_replace('{type}', 'wistia', $template);
			$replace = str_replace('{embed}', $embed, $replace);
			$replace = str_replace('{video}', 'javascript:;', $replace);
			$replace = str_replace('data-fancybox', 'data-fancybox="iframe" data-type="iframe" data-src="//fast.wistia.net/embed/iframe/' . $embed . '?autoplay=1"', $replace);

			$buffer = str_replace($wistia, $replace, $buffer);
		}
	}
}


/**
 * wordkeeper_process_buffer_images function.
 *
 * @access public
 * @param mixed $buffer
 * @param mixed $images
 * @return string $buffer
 */
function wordkeeper_process_buffer_images(&$buffer, &$images) {
	// Skip feature if feature is disabled by overrides
	$feature = apply_filters('wordkeeper_filter_overrides', __FUNCTION__, $buffer);
	if($feature === false) {
		return;
	}

	foreach($images as $image) {
		wordkeeper_lazyload_image($image, $buffer);
	}
}


/**
 * wordkeeper_process_buffer_videos function.
 *
 * @access public
 * @param mixed $buffer
 * @param mixed $frames
 * @return string $buffer
 */
function wordkeeper_process_buffer_videos(&$buffer, &$frames) {
	// Skip feature if feature is disabled by overrides
	$feature = apply_filters('wordkeeper_filter_overrides', __FUNCTION__, $buffer);
	if($feature === false) {
		return;
	}

	foreach($frames['videos'] as $video) {
		wordkeeper_lazyload_video($video, $buffer);
	}
}


/**
 * wordkeeper_process_buffer_iframes function.
 *
 * @access public
 * @param mixed $buffer
 * @param mixed $frames
 * @return string $buffer
 */
function wordkeeper_process_buffer_iframes(&$buffer, &$frames) {
	// Skip feature if feature is disabled by overrides
	$feature = apply_filters('wordkeeper_filter_overrides', __FUNCTION__, $buffer);
	if($feature === false) {
		return;
	}

	foreach($frames['iframes'] as $frame) {
		if(strpos($buffer, '<noscript>' . $frame) === false) {
			wordkeeper_lazyload_iframe($frame, $buffer);
		}
	}
}


/**
 * wordkeeper_defer_buffer_scripts function.
 *
 * @access public
 * @param mixed $buffer
 * @param mixed $scripts
 * @return string $buffer
 */
function wordkeeper_defer_buffer_scripts(&$buffer, &$scripts) {
	// Skip feature if feature is disabled by overrides
	$feature = apply_filters('wordkeeper_filter_overrides', __FUNCTION__, $buffer);
	if($feature === false) {
		return $buffer;
	}

	foreach($scripts as $script) {
		if (
			strpos($script, '.google-analytics.com') === false &&
			strpos($script, 'www.googleadservices.com') === false &&
			strpos($script, 'connect.facebook.net') === false &&
			strpos($script, 'var google_conversion_id') === false &&
			strpos($script, 'www.formstack.com') === false &&
			(strpos($script, '/jquery.js') === false || strpos($script, '/jquery.json') !== false) &&
			strpos($script, '/jquery.min.js') === false &&
			strpos($script, '/jquery-migrate.js') === false &&
			strpos($script, '/jquery-migrate.min.js') === false &&
			strpos($script, 'try{jQuery.noConflict();}catch(e){};') === false //&&
			//(strpos($script, 'type=') === false || preg_match('#type=[\'"]text/javascript[\'"]#', $script) === 1)
		) {
			preg_match_all('/<\s?script.*?\s?>/', $script, $begin);

			if ($begin[0][0] == '<script>') {;
				$replace = str_replace('<script>', '<script defer="defer">', $script);
				$buffer = str_replace($script, $replace, $buffer);
			}
			elseif (
				strpos($begin[0][0], 'defer=') === false &&
				strpos($begin[0][0], ' async') === false &&
				preg_match('# data-cfasync=[\'"]*false#', $begin[0][0]) == 0
			) {
				$replace = str_replace('<script ', '<script defer="defer" ', $begin[0]);
				$replace = str_replace($begin[0], $replace, $script);
				$buffer = str_replace($script, $replace, $buffer);
			}
		}
	}

	return $buffer;
}


/**
 * wordkeeper_minify_js function.
 *
 * @access public
 * @param mixed $buffer
 * @return void
 */
function wordkeeper_minify_js(&$buffer) {
	preg_match_all('/(?s)<\s?script.*?(?:\/\s?>|<\s?\/\s?script\s?>)/', $buffer, $scripts);

	foreach($scripts[0] as $script) {
		if(strpos($script, '.js') !== false && strpos($script, 'src=') !== false) {
			preg_match('#src=[\'"]([^\'"]*)[\'"]#', $script, $src);
			if(!empty($src) && !empty($src[1])) {
				$uri = parse_url($src[1]);
				$host = '';
				if(isset($uri['host']) || strpos($host, '.') === false) {
					$host = $uri['host'];
				}

				if(empty($host) || $host == $_SERVER['HTTP_HOST']) {
					if(is_multisite()) {
						$cache_root = '/wp-content/cache/minify/' . get_current_blog_id();
					}
					else {
						$cache_root = '/wp-content/cache/minify';
					}

					$cache_file = rtrim(ABSPATH, '/') . $cache_root . $uri['path'];

					if(!file_exists($cache_file)) {
						$scheme = (is_ssl()) ? 'https' : 'http';
						$minify_url = plugin_dir_url(__FILE__) . 'minify/?f=' . $uri['path'];
						$minified_result = wp_safe_remote_get($minify_url);

						if(200 !== wp_remote_retrieve_response_code($minified_result)) {
							continue;
						}

						$content = wp_remote_retrieve_body($minified_result);
						if(!file_exists(dirname($cache_file))) {
							mkdir(dirname($cache_file), 0755, true);
						}
						file_put_contents($cache_file, $content);
					}

					$replace = str_replace($uri['path'], $cache_root . $uri['path'], $script);
					$cleanpath = str_replace('//', '/', $uri['path']);
					$replace = str_replace($uri['path'], $cleanpath, $replace);
					$buffer = str_replace($script, $replace, $buffer);
				}
			}
		}
	}
}


/**
 * wordkeeper_minify_css function.
 *
 * @access public
 * @param mixed $buffer
 * @return void
 */
function wordkeeper_minify_css(&$buffer) {
	preg_match_all('/(?s)<\s?link.*?(?:\/\s?>|<\s?\/\s?link\s?>)/', $buffer, $links);

	foreach($links[0] as $link) {
		if(strpos($link, '.css') !== false && strpos($link, 'href=') !== false) {
			preg_match('#href=[\'"]([^\'"]*)[\'"]#', $link, $src);
			if(!empty($src) && !empty($src[1])) {
				$uri = parse_url($src[1]);
				$host = '';
				if(isset($uri['host']) || strpos($host, '.') === false) {
					$host = $uri['host'];
				}

				if(empty($host) || $host == $_SERVER['HTTP_HOST']) {
					if(is_multisite()) {
						$cache_root = '/wp-content/cache/minify/' . get_current_blog_id();
					}
					else {
						$cache_root = '/wp-content/cache/minify';
					}

					$cache_file = rtrim(ABSPATH, '/') . $cache_root . $uri['path'];

					if(!file_exists($cache_file)) {
						$scheme = (is_ssl()) ? 'https' : 'http';
						$minify_url = plugin_dir_url(__FILE__) . 'minify/?f=' . $uri['path'];
						$minified_result = wp_safe_remote_get($minify_url);

						if(200 !== wp_remote_retrieve_response_code($minified_result)) {
							continue;
						}

						$content = wp_remote_retrieve_body($minified_result);
						if(!file_exists(dirname($cache_file))) {
							mkdir(dirname($cache_file), 0755, true);
						}
						file_put_contents($cache_file, $content);
					}

					$replace = str_replace($uri['path'], $cache_root . $uri['path'], $link);
					$cleanpath = str_replace('//', '/', $uri['path']);
					$replace = str_replace($uri['path'], $cleanpath, $replace);
					$buffer = str_replace($link, $replace, $buffer);
				}
			}
		}
	}
}


/**
 * wordkeeper_apply_deferscript function.
 *
 * @access public
 * @param mixed $buffer
 * @return string $buffer
 */
function wordkeeper_apply_deferscript(&$buffer) {
	global $wordkeeper;

	// Skip feature if feature is disabled by overrides
	$feature = apply_filters('wordkeeper_filter_overrides', __FUNCTION__, $buffer);
	if($feature === false) {
		return;
	}

	if($_SERVER['SCRIPT_NAME'] == '/index.php') {
		if(wp_script_is('jquery')) {
			if(strpos($buffer, 'wordkeeper_lazyload_scripts') === false){
				$deferscript = '(function($){$(window).load(function(e){setTimeout(\'wordkeeper_lazyload_scripts();\',' . $wordkeeper['deferscript-delay'] . ');});})(jQuery);function wordkeeper_lazyload_scripts(){jQuery(function(){if(typeof $==\'undefined\'){$=jQuery;} $(\'[type="text/deferscript"]\').each(function(index,item){$item=$(item);if(typeof $item.attr(\'src\')!=\'undefined\'){$.getScript($item.attr(\'src\'));}
		else{var replace=item.outerHTML.replace(\'deferscript\',\'javascript\');$item.replaceWith(replace);}});});}';
				$buffer = str_replace('</body>', '<script>' . $deferscript . "</script>\n</body>", $buffer);
			}
		}
	}
}


/**
 * wordkeeper_strip_duplicate_assets function.
 *
 * @access public
 * @param mixed $buffer
 * @param mixed $assets
 * @return string $buffer
 */
function wordkeeper_strip_duplicate_assets(&$buffer, $assets) {
	$items = array();

	foreach($assets as $asset) {
		if(strpos($asset, '<link') !== false) {
			preg_match('#href=[\'"]([^\'"]*)[\'"]#', $asset, $item);
		}
		elseif(strpos($asset, '<script') !== false) {
			preg_match('#src=[\'"]([^\'"]*)[\'"]#', $asset, $item);
		}

		if(!empty($item)) {
			$md5 = md5($item[1]);

			if(isset($items[$md5])) {
				$buffer = str_replace($asset, '', $buffer);
			}
			$items[$md5] = true;
		}
	}

	unset($items);
}


/**
 * wordkeeper_strip_session_cookie function.
 *
 * Strip PHPSESSID from cookies if present in headers
 *
 * @access public
 * @return void
 */
function wordkeeper_strip_session_cookie() {
	$cookies = array();
	$session = false;
	if (!headers_sent()) {
		foreach (headers_list() as $header) {
			if (stripos($header, 'Set-Cookie:') !== false) {
				if(strpos($header, 'PHPSESSID') !== false) {
					$session = true;
				}
				else {
					$cookies[] = $header;
				}
			}
		}
	}

	if($session) {
		header_remove('Set-Cookie');
		if(!empty($cookies)) {
			foreach($cookies as $cookie) {
				header($cookie);
			}
		}
	}
}


/**
 * wordkeeper_strip_comments function.
 *
 * @access public
 * @param mixed $buffer
 * @return string $buffer
 */
function wordkeeper_strip_comments(&$buffer) {
	$buffer = preg_replace('/<!--(.*)-->/Uis', '', $buffer);
}


/**
 * wordkeeper_remove_google_maps function.
 *
 * @access public
 * @param mixed $buffer
 * @return string $buffer
 */
function wordkeeper_remove_google_maps(&$buffer) {
	$buffer = preg_replace('#<script[^>]*src=[\'"]*(?:http[s]*:)*//maps\.google(?:apis)*\.com/maps/api/js[^>]*>[\s\t\r\n]*</script>#', '', $buffer);
	$buffer = preg_replace('#<link[^>]*rel=[\'"]*dns-prefetch[\'"]*[^>]href=[\'"]*(?:http[s]*:)*//maps\.google(?:apis)*\.com[^>]*/>#', '', $buffer);
}


/**
 * wordkeeper_concatenate_google_fonts function.
 *
 * @access public
 * @param mixed $buffer
 * @return void
 */
function wordkeeper_concatenate_google_fonts(&$buffer) {
	// Get all Google Fonts CSS files.
	//$buffer_without_comments = preg_replace('/<!--(.*)-->/Uis', '', $buffer);
	preg_match_all('/<link(?:\s.+)?\shref\s*=\s*(\'|")((?!\1).+fonts\.googleapis\.com(?!\1).+)\1(?:\s.*)?>/iU', $buffer, $matches);

	$i = 0;
	$fonts   = array();
	$subsets = array();

	if(!$matches[2]) {
		return;
	}

	foreach($matches[2] as $index => $font) {
		if(!preg_match( '/rel=["\']dns-prefetch["\']/', $matches[0][$i])) {
			// Get fonts name.
			$font = str_replace(array('%7C', '%7c'), '|', $font);
			$font = explode('family=', $font);
			$font = (isset($font[1])) ? explode('&', $font[1]) : array();

			// Add font to the collection.
		    $fonts = array_merge($fonts, explode('|', reset($font)));

		    // Add subset to collection.
			$subset = (is_array($font)) ? end($font) : '';
		    if(false !== strpos($subset, 'subset=')) {
				$subset  = explode('subset=', $subset);
				$subsets = array_merge($subsets, explode(',', $subset[1]));
		    }

		    // Delete the Google Fonts tag.
		    if($index == 0) {
			    $buffer = str_replace($matches[0][$i], '{google-fonts}', $buffer);
		    }
		    else {
			    $buffer = str_replace($matches[0][$i], '', $buffer);
			}
		}

	    $i++;
	}

	// Concatenate fonts tag.
	$subsets = ($subsets) ? '&subset=' . implode(',', array_filter(array_unique($subsets))) : '';
	$fonts   = trim(implode('|' , array_filter(array_unique($fonts))), '|');
	$fonts	 = str_replace('|', '%7C', $fonts);

	if(!empty($fonts)) {
		$fonts   = '<link rel="stylesheet" href="//fonts.googleapis.com/css?family=' . $fonts . $subsets . '" />';
		$buffer = str_replace('{google-fonts}', $fonts, $buffer);
	}
	else{
		$buffer = str_replace('{google-fonts}', '', $buffer);
	}
}


/**
 * wordkeeper_remove_query_strings function.
 *
 * @since    1.0.0
 * @access public
 * @param mixed $src
 * @return void
 */
function wordkeeper_remove_query_strings($src) {

	if (!is_admin()) {
		$siteurl = get_site_url();
		$siteurl = parse_url($siteurl);
		$host = $_SERVER['HTTP_HOST'];

		if (strpos($src, '.php') === false && strpos($src, $host) !== false) {
			$parts = explode('?', $src);
			$path = $parts[0];
			return $path;
		}
		else {
			return $src;
		}
	}

	return $src;
}


/**
 * wordkeeper_heartbeat_control function.
 *
 * @since    1.0.0
 * @access public
 * @return void
 */
function wordkeeper_heartbeat_control() {
	global $pagenow;

	if($pagenow != 'post.php' && $pagenow != 'post-new.php') {
		wp_deregister_script('heartbeat');
	}
}


/**
 * wordkeeper_heartbeat_frequency function.
 *
 * @since    1.0.0
 * @access public
 * @param mixed $settings
 * @return void
 */
function wordkeeper_heartbeat_frequency($settings) {
	$settings['interval'] = 60;
	return $settings;
}

/**
 * wordkeeper_apply_resource_hints function.
 *
 * @since    1.0.0
 * @access public
 * @param mixed $buffer
 * @return void
 */
function wordkeeper_apply_resource_hints(&$buffer) {
	preg_match_all('/(?s)<\s?script.*?(?:\/\s?>|<\s?\/\s?script\s?>)/', $buffer, $scripts);
	preg_match_all('/(?s)<\s?link.*?(?:\/\s?>|<\s?\/\s?link\s?>)/', $buffer, $links);

	$type = 'script';
	foreach($scripts[0] as $resource) {
		$src = '';
		if(strpos($resource, 'src') !== false) {
			preg_match('#src=[\'"]*([^\'"]+)[\'"]#', $resource, $src);
			$src = (!empty($src)) ? $src[1] : '';
			$uri = parse_url($src);
			$host = '';
			if(isset($uri['host']) || strpos($host, '.') === false) {
				$host = $uri['host'];
			}

			if(empty($host) || $host == $_SERVER['HTTP_HOST']) {	
				$src = ('//' === substr($src, 0, 2)) ? preg_replace('/^\/\/([^\/]*)\//', '/', $src) : preg_replace('/^http(s)?:\/\/[^\/]*/', '', $src);
			}
			$src = trim($src);			
			wordkeeper_add_resource_hint_header($src, $type);
		}
	}

	$type = 'style';
	foreach($links[0] as $resource) {
		$src = '';
		if(strpos($resource, 'stylesheet') !== false || strpos($resource, '.css') !== false) {
			preg_match('#href=[\'"]*([^\'"]+)[\'"]#', $resource, $src);
			$src = (!empty($src)) ? $src[1] : '';
			$uri = parse_url($src);
			$host = '';
			if(isset($uri['host']) || strpos($host, '.') === false) {
				$host = $uri['host'];
			}

			if(empty($host) || $host == $_SERVER['HTTP_HOST']) {			
				$src = ('//' === substr($src, 0, 2)) ? preg_replace('/^\/\/([^\/]*)\//', '/', $src) : preg_replace('/^http(s)?:\/\/[^\/]*/', '', $src);
			}
			$src = trim($src);
			wordkeeper_add_resource_hint_header($src, $type);
		}
	}
}


/**
 * wordkeeper_apply_resource_hints function.
 *
 * @since    1.0.0
 * @access public
 * @param mixed $src
 * @return void
 */
function wordkeeper_add_resource_hint_header($src, $type) {
	static $header_size = 0;

	if(!empty($src)) {
		$header = sprintf(
			'Link: <%s>; rel=preload; as=%s',
			esc_url($src),
			$type
		);

		// Ensure that the header is within reasonable bounds
		if(($header_size + strlen($header)) < 4096) {
			$header_size += strlen($header);
			header($header, false);
		}
	}
}


/**
 * wordkeeper_remove_wordpress_generator function.
 *
 * Disable the WP generator tag
 *
 * @access public
 * @return void
 */
function wordkeeper_remove_wordpress_generator() {
	return '';
}


/**
 * wordkeeper_remove_revslider_generator function.
 *
 * Disable the Revolution Slider generator tag
 *
 * @access public
 * @return void
 */
function wordkeeper_remove_revslider_generator() {
	return '';
}


/**
 * wordkeeper_remove_visual_composer_generator function.
 *
 * Disable the Visual Composer generator tag
 *
 * @access public
 * @return void
 */
function wordkeeper_remove_visual_composer_generator() {
	if(function_exists('visual_composer')) {
		remove_action('wp_head', array(visual_composer(), 'addMetaData'));
	}
}


/**
 * wordkeeper_remove_woocommerce_generator function.
 *
 * Disable the WooCommerce generator tag
 *
 * @access public
 * @return void
 */
function wordkeeper_remove_woocommerce_generator() {
	remove_action('wp_head','wc_generator_tag');
}


/**
 * wordkeeper_disable_emojis function.
 *
 * Disable Emojis
 *
 * @access public
 * @return void
 */
function wordkeeper_disable_emojis() {
	remove_action('wp_head', 'print_emoji_detection_script', 7);
	remove_action('admin_print_scripts', 'print_emoji_detection_script');
	remove_action('wp_print_styles', 'print_emoji_styles');
	remove_action('admin_print_styles', 'print_emoji_styles');
	remove_filter('the_content_feed', 'wp_staticize_emoji');
	remove_filter('comment_text_rss', 'wp_staticize_emoji');
	remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
}


/**
 * wordkeeper_disable_tinymce_emojis function.
 *
 * Disable Emojis in TinyMCE
 *
 * @access public
 * @param mixed $plugins
 * @return void
 */
function wordkeeper_disable_tinymce_emojis($plugins) {
	if(is_array($plugins)) {
		return array_diff($plugins, array('wpemoji'));
	}
	else {
		return array();
	}
}


/**
 * wordkeeper_disable_emojis_dns_prefetch function.
 *
 * Disable Emoji DNS prefetch
 *
 * @access public
 * @param mixed $urls
 * @param mixed $relation_type
 * @return void
 */
function wordkeeper_disable_emojis_dns_prefetch($urls, $relation_type) {
	if('dns-prefetch' == $relation_type) {
		/** This filter is documented in wp-includes/formatting.php */
		$emoji_svg_url = apply_filters('emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/');

		$urls = array_diff($urls, array($emoji_svg_url));
	}

	return $urls;
}


/**
 * wordkeeper_disable_embeds function.
 *
 * Disable oEmbed support
 *
 * @access public
 * @return void
 */
function wordkeeper_disable_embeds() {

	// Remove the REST API endpoint.
	remove_action('rest_api_init', 'wp_oembed_register_route');

	// Turn off oEmbed auto discovery.
	add_filter('embed_oembed_discover', '__return_false');

	// Don't filter oEmbed results.
	remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);

	// Remove oEmbed discovery links.
	remove_action('wp_head', 'wp_oembed_add_discovery_links');

	// Remove oEmbed-specific JavaScript from the front-end and back-end.
	remove_action('wp_head', 'wp_oembed_add_host_js');

	// Remove filter of the oEmbed result before any HTTP requests are made.
	remove_filter('pre_oembed_result', 'wp_filter_pre_oembed_result', 10);
}


/**
 * wordkeeper_disable_tinymce_embeds function.
 *
 * Disable TinyMCE oEmbed support
 *
 * @access public
 * @param mixed $plugins
 * @return void
 */
function wordkeeper_disable_tinymce_embeds($plugins) {
	return array_diff($plugins, array('wpembed'));
}


/**
 * Disable oEmbed rewrites
 *
 * @since    1.0.0
 */
function wordkeeper_disable_embed_rewrites($rules) {
	foreach($rules as $rule => $rewrite) {
		if(false !== strpos($rewrite, 'embed=true')) {
			unset($rules[$rule]);
		}
	}

	return $rules;
}


/**
 * Sets the 'no_found_rows' param to true.
 *
 * Replace SQL_CALC_ROWS with no_found_rows to prevent slow queries
 *
 * @param  WP_Query $wp_query The WP_Query instance. Passed by reference.
 * @return void
 */
function wordkeeper_db_set_no_found_rows(\WP_Query $wp_query) {
	$wp_query->set('no_found_rows', true);
}


/**
 *
 * Handle pagination correctly when replacing count queries with COUNT * instead of CALC_FOUND_ROWS
 *
 * @param array    $clauses  Array of clauses that make up the SQL query.
 * @param WP_Query $wp_query The WP_Query instance. Passed by reference.
 * @return array
 */
function wordkeeper_db_set_found_posts($clauses, \WP_Query $wp_query) {
	// Ignore this for queries that don't involve multiple posts
	if ($wp_query->is_singular()) {
		return $clauses;
	}

	global $wpdb;

	// Determine if there are any query clauses to account for
	$where = isset($clauses[ 'where' ]) ? $clauses[ 'where' ] : '';
	$join = isset($clauses[ 'join' ]) ? $clauses[ 'join' ] : '';
	$distinct = isset($clauses[ 'distinct' ]) ? $clauses[ 'distinct' ] : '';

	// Replace SQL_CALC_ROWS with COUNT *
	$wp_query->found_posts = $wpdb->get_var("SELECT $distinct COUNT(*) FROM {$wpdb->posts} $join WHERE 1=1 $where");

	// Determine posts per page
	$posts_per_page = (!empty($wp_query->query_vars['posts_per_page']) ? absint($wp_query->query_vars['posts_per_page']) : absint(get_option('posts_per_page')));

	// Set the number of max pages
	$wp_query->max_num_pages = ceil($wp_query->found_posts / $posts_per_page);

	// Return $clauses so query can run.
	return $clauses;
}

// Capture output for processing before flushed to web server
if($wordkeeper['use-buffering'] === true) {
	add_action('init', 'wordkeeper_buffer_start', 0);
	add_action('shutdown', 'wordkeeper_buffer_stop', 1000);
}

// Adjust heartbeat frequency
add_action('init', 'wordkeeper_heartbeat_control', 0);
add_filter('heartbeat_settings', 'wordkeeper_heartbeat_frequency');

// Add resource hints, run late so that other JS/CSS optimizations are done
if($wordkeeper['use-buffering'] === true) {
	add_filter('wordkeeper_after_buffer_edit', 'wordkeeper_apply_resource_hints', 100, 1);
}

// Minify JavaScript
if($wordkeeper['minify-js'] === true) {
	add_filter('wordkeeper_buffer_scripts_edit', 'wordkeeper_minify_js', 10, 1);
}

// Minify CSS
if($wordkeeper['minify-css'] === true) {
	add_filter('wordkeeper_buffer_links_edit', 'wordkeeper_minify_css', 10, 1);
}

// Lazy load videos
if($wordkeeper['lazyload-videos'] === true) {
	if(!$wordkeeper['amp']) {
		add_filter('wordkeeper_before_buffer_edit', 'wordkeeper_lazyload_script_videos', 1, 1);
		add_filter('wordkeeper_buffer_iframes_edit', 'wordkeeper_process_buffer_videos', 2, 2);
	}

	wp_enqueue_style('wordkeeper-lazyload-video', plugin_dir_url(__FILE__) . 'css/lazyload.video.min.css');

	if($wordkeeper['lazyload-videos-mode'] == 'default') {
		wp_enqueue_script('wordkeeper-lazyload-video', plugin_dir_url(__FILE__) . 'js/lazyload.video.min.js', array('jquery'));
	}
	else {
		wp_enqueue_style('wordkeeper-lightbox-video', plugin_dir_url(__FILE__) . 'css/jquery.fancybox.min.css');
		wp_enqueue_script('wordkeeper-lightbox-video', plugin_dir_url(__FILE__) . 'js/jquery.fancybox.min.js', array('jquery'));
		wp_enqueue_script('wordkeeper-lazyload-video', plugin_dir_url(__FILE__) . 'js/lightbox.video.min.js', array('jquery'));
	}
}

// Lazy load images
if($wordkeeper['lazyload-images'] === true) {
	if(!$wordkeeper['amp']) {
		add_filter('wordkeeper_buffer_images_edit', 'wordkeeper_process_buffer_images', 2, 2);
	}

	wp_enqueue_script('wordkeeper-lazyload', plugin_dir_url(__FILE__) . 'js/lazysizes.min.js', array('jquery'));
	if($wordkeeper['lazyload-mode'] != 'near') {
		if(function_exists('wp_add_inline_script')) {
			if($wordkeeper['lazyload-mode'] == 'none') {
				$lazymode = "window.lazySizesConfig = window.lazySizesConfig || {};\nlazySizesConfig.loadMode = 0;";
			}
			else {
				$lazymode = "window.lazySizesConfig = window.lazySizesConfig || {};\nlazySizesConfig.loadMode = 1; lazySizesConfig.expand = 50;";
			}
			wp_add_inline_script('wordkeeper-lazyload', $lazymode, 'before');
			$wordkeeper['lazy-loaded'] = true;
			unset($lazymode);
		}
	}
}

// Lazy load iframes
if($wordkeeper['lazyload-iframes'] === true) {
	if(!$wordkeeper['amp']) {
		add_filter('wordkeeper_buffer_iframes_edit', 'wordkeeper_process_buffer_iframes', 2, 2);
	}

	wp_enqueue_script('wordkeeper-lazyload', plugin_dir_url(__FILE__) . 'js/lazysizes.min.js', array('jquery'));
	if(!isset($wordkeeper['lazy-loaded'])) {
		if($wordkeeper['lazyload-mode'] != 'near') {
			if(function_exists('wp_add_inline_script')) {
				if($wordkeeper['lazyload-mode'] == 'none') {
					$lazymode = "window.lazySizesConfig = window.lazySizesConfig || {};\nlazySizesConfig.loadMode = 0;";
				}
				else {
					$lazymode = "window.lazySizesConfig = window.lazySizesConfig || {};\nlazySizesConfig.loadMode = 1; lazySizesConfig.expand = 50;";
				}
				wp_add_inline_script('wordkeeper-lazyload', $lazymode, 'before');
				unset($lazymode);
			}
		}
	}
}

// Defer JavaScript if not an AMP page
if($wordkeeper['defer-javascript'] === true) {
	if(!$wordkeeper['amp']) {
		add_filter('wordkeeper_buffer_scripts_edit', 'wordkeeper_defer_buffer_scripts', 2, 2);
	}
}

// Use custom deferscript loader to async load problematic scripts
if($wordkeeper['use-deferscript'] === true) {
	if(!$wordkeeper['amp']) {
		add_filter('wordkeeper_after_buffer_edit', 'wordkeeper_apply_deferscript', 1, 1);
	}
}

// Optimize/concatenate Google fonts
if($wordkeeper['optimize-google-fonts'] === true) {
	add_filter('wordkeeper_before_buffer_edit', 'wordkeeper_concatenate_google_fonts', 2, 1);
}

// Strip duplicate JS and CSS files
if($wordkeeper['strip-duplicate-assets'] === true) {
	add_filter('wordkeeper_buffer_scripts_edit', 'wordkeeper_strip_duplicate_assets', 2, 2);
	add_filter('wordkeeper_buffer_links_edit', 'wordkeeper_strip_duplicate_assets', 2, 2);
}

// Strip HTML comments
if($wordkeeper['strip-comments'] === true) {
	add_filter('wordkeeper_before_buffer_edit', 'wordkeeper_strip_comments', 3, 1);
}

// Strip PHPSESSID from headers if present
if($wordkeeper['strip-session-cookie'] === true) {
	add_action('send_headers', 'wordkeeper_strip_session_cookie');
}

// Remove Google Maps API output
if($wordkeeper['disable-maps-api'] === true) {
	add_filter('wordkeeper_before_buffer_edit', 'wordkeeper_remove_google_maps', 4, 1);
}

// Remove query strings from static resources
if($wordkeeper['remove-query-strings'] === true) {
	add_filter('script_loader_src', 'wordkeeper_remove_query_strings', 15, 1);
	add_filter('style_loader_src', 'wordkeeper_remove_query_strings', 15, 1);
}

// Remove Emoji support
if($wordkeeper['disable-emojis'] === true) {
	add_action('init', 'wordkeeper_disable_emojis');
	add_filter('tiny_mce_plugins', 'wordkeeper_disable_tinymce_emojis');
	add_filter('wp_resource_hints', 'wordkeeper_disable_emojis_dns_prefetch', 10, 2);
}

// Remove embed support
if($wordkeeper['disable-wordpress-embeds'] === true) {
	add_action('init', 'wordkeeper_disable_embeds', 9999);
	add_filter('tiny_mce_plugins', 'wordkeeper_disable_tinymce_embeds');
	add_filter('rewrite_rules_array', 'wordkeeper_disable_embed_rewrites');
}

// Clean up various forms of garbage output that jeopardizes security and speed
if($wordkeeper['disable-header-garbage'] === true) {
	add_filter('the_generator', 'wordkeeper_remove_wordpress_generator');
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wp_shortlink_wp_head');
	remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');

	// Remove WooCommerce generator
	add_action('get_header', 'wordkeeper_remove_woocommerce_generator');

	// Remove Visual Composer generator
	add_action('init', 'wordkeeper_remove_visual_composer_generator', 100);

	// Remove RevSlider generator
	add_filter('revslider_meta_generator', 'wordkeeper_remove_revslider_generator');
}

// Optimize pagination queries by replacing SQL_CALC_ROWS
if($wordkeeper['optimize-db-count-queries'] === true) {
	// Sets no found rows to replace SQL_CALC_ROWS
	add_filter('pre_get_posts', 'wordkeeper_db_set_no_found_rows', 10, 1);

	// Sets replacement for SQL_CALC_ROWS to COUNT * and accounts for paginated queries
	add_filter('posts_clauses', 'wordkeeper_db_set_found_posts', 10, 2);
}

// Remove WooCommerce get_refreshed_fragments calls from occurring by default
if($wordkeeper['disable-woocommerce-refresh-fragments'] === true) {
	// Only run if WooCommerce is active
	if(in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
		/** Disable Ajax Call from WooCommerce */
		add_action( 'wp_enqueue_scripts', 'wordkeeper_dequeue_woocommerce_cart_fragments', 11);
		function wordkeeper_dequeue_woocommerce_cart_fragments() { wp_dequeue_script('wc-cart-fragments'); }
	}
}

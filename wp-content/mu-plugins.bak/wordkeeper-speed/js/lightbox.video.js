var wordkeeper_video = {};

(function ($) {
	$((function() {

	  var youtube = $('[data-type="youtube"]');

	  $.each(youtube, function(index, item){
			item = $(item);

			var source = 'https://img.youtube.com/vi/' + item.data('embed') + '/sddefault.jpg';
			item.css('background', 'url("' + source + '") no-repeat center center');
			item.css('background-size', 'cover');
	  });

	  var vimeo = $('[data-type="vimeo"]');

	  $.each(vimeo, function(index, item){
		item = $(item);

	    $.ajax({
		    url: 'https://vimeo.com/api/v2/video/' + item.data('embed') + '.json',
		    dataType: 'json',
		    success: function(data) {
			  	var source = data[0].thumbnail_large;
				item.css('background', 'url("' + source + '") no-repeat center center');
				item.css('background-size', 'cover');
		    }
	    });
	  });

	  var wistia = $('[data-type="wistia"]');

	  $.each(wistia, function(index, item){
	  	item = $(item);

	    $.ajax({
		    url: 'https://fast.wistia.com/oembed?url=' + encodeURI('https://fast.wistia.net/embed/iframe/' + item.data('embed')),
		    dataType: 'json',
		    success: function(data) {
			  	var source = data.thumbnail_url;
				item.css('background', 'url("' + source + '") no-repeat center center');
				item.css('background-size', 'cover');
		    }
	    });
	  });
	}));
})(jQuery);
